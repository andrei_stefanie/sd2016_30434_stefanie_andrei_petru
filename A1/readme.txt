Installation Instructions:

1 Java

1.1 Java JDK and JRE
1) Access the next link:
http://www.oracle.com/technetwork/java/javase/downloads/index.html
2) Click on the icon which is above Java Platform (JDK). You will be redirected to Java
downloads.
3) Click on the link Accept License Agreement.
4) Click on the link which corresponds to your version of the Operating System. In the
example the version which is used corresponds to Windows x64 and the file is named
jdk-8u51-windows-x64.exe.
5) After java-version.exe is pressed, a file with the same name will be downloaded.
6) Click on java-version.exe.
7) You will be asked the next question: Do you want to allow the following program to
make changes to this computer? Click Yes.
8) Click Next.
9) You will be asked where you want to install Java. Use the default location and click Next.
10) After the JDK is installed, you will be asked where you want to install the JRE. Use the
default location and click Next.

1.2 Set JAVA_HOME and JAVA_JRE variables
1) Click Start.
2) Right-Click on Computer.
3) Select Properties.
4) Click on Advanced System Settings.
5) Click on Environment Variables.
6) Under System Variables click New.
7) In the text field associated with the name of the variable insert JAVA_HOME and in the
field associated with the value of the variable insert C:\Program Files\Java\java_version;.
8) Click OK.
9) Under System Variables click New again.
10) In the text field associated with the name of the variable insert JRE_HOME and in the
field associated with the value of the variable insert C:\Program Files\Java\java_version;.
11)  Click OK.


2 WAMP

1) Download WAMP from:
http://www.wampserver.com/en/#download-wrapper  (preferably 64bit version)
2) Next
3) Accept the agreement
4) Select the installation location (can use the default c:\\)
5) Next
6) Next
7) Setup your mail
8) Next (localhost is default)
9) Just press open (it searches for file explorer - "My Computer")
10) Installation is complete, you can start it.
11) It will be used to create a MySQL server and for the front-end part


3 Web Server

3.1 Tomcat
1) Click on the next link: https://tomcat.apache.org/download-70.cgi. (It should work with other versions of tomcat too)
2) Under Binary Distributions look for Core and click on zip.
3) A file called apache-tomcat-version.zip is downloaded.
4) Extract the content of this file on C:\. The file startup.bat should be at the location
C:\apache-tomcat-7.0.24\bin.
3.2 Set the CATALINA_HOME variable
1) Click Start.
2) Right-Click on Computer.
3) Select Properties.
4) Click on Advanced System Settings.
5) Click on Environment Variables.
6) Under System Variables click New.
7) In the text field associated with the name of the variable insert CATALINA_HOME and
in the field associated with the value of the variable insert C:\apache-tomcat-version;.
8) Click OK
9) It will be used to create the server for the back-end part


Setting up the application:
- Start WAMP application (it will appear in the notification area of the taskbar)
- Left click on it
- Go to MySQL and click on MySQL Console (password should be empty)
- Paste the following script in it


4 Create the Bank database - 

MySQL Script:
----------------------------Start of MySQL Scrip------------------------------

CREATE DATABASE IF NOT EXISTS `bank` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bank`;

CREATE TABLE IF NOT EXISTS `account` (
  `idaccount` int(11) NOT NULL AUTO_INCREMENT,
  `idowner` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `balance` int(11) DEFAULT NULL,
  `creation_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idaccount`),
  UNIQUE KEY `idaccount_UNIQUE` (`idaccount`),
  KEY `accountOwner_idx` (`idowner`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `admin` (
  `idadmin` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idadmin`),
  UNIQUE KEY `idadmin_UNIQUE` (`idadmin`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

INSERT INTO `admin` (`idadmin`, `username`, `password`) VALUES
(1, 'admin', 'admin');

CREATE TABLE IF NOT EXISTS `client` (
  `idclient` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `cnp` varchar(13) NOT NULL,
  `address` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idclient`),
  UNIQUE KEY `cnp_UNIQUE` (`cnp`),
  UNIQUE KEY `idclient_UNIQUE` (`idclient`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `employee` (
  `idemployee` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `employedSince` date DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idemployee`),
  UNIQUE KEY `idemployee_UNIQUE` (`idemployee`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `log_bill` (
  `idlog_bill` int(11) NOT NULL AUTO_INCREMENT,
  `bill_nr` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `idaccount` int(11) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idlog_bill`),
  KEY `payed_from_idx` (`idaccount`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `log_employee` (
  `idemployee` int(11) NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `description` mediumtext,
  `idlog` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idlog`),
  KEY `employee_idx` (`idemployee`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `account`
  ADD CONSTRAINT `accountOwner` FOREIGN KEY (`idowner`) REFERENCES `client` (`idclient`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `log_bill`
  ADD CONSTRAINT `payed_from` FOREIGN KEY (`idaccount`) REFERENCES `account` (`idaccount`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `log_employee`
  ADD CONSTRAINT `employee` FOREIGN KEY (`idemployee`) REFERENCES `employee` (`idemployee`) ON DELETE CASCADE ON UPDATE CASCADE;

  ---------------------End of MySQL script--------------------------


5 Set up the front-end part
- Open the Front directory from the source files
- Copy the app directory to C:\wamp\www (if wamp was installed in the default location)
- Click on WAMP and Restart All Services


6 Set up the back-end part
- Open the Back directory from the source files
- Copy the Bank directory to C:\apache-tomcat-7.0.65\webapps (if tomcat was installed in the root of C drive)
- Start the Tomcat server by navigating to C:\apache-tomcat-7.0.65\bin and run startup.bat


The application can be used by accessing http://localhost/app/ from a browser
Default manager account is
username: admin
password: admin