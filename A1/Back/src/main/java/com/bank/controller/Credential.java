package com.bank.controller;

/**
 * User: Stefa
 * Date: 02-Apr-16
 * Time: 11:59
 */
class Credential
{
    private String username, password;
    private int id;

    Credential(String username, String password, int id)
    {
        this.username = username;
        this.password = password;
        this.id = id;
    }

    String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    int getId()
    {
        return id;
    }

    void setId(int id)
    {
        this.id = id;
    }

    public boolean equals(Object object)
    {
        if (this == object) return true;
        if (!(object instanceof Credential)) return false;

        Credential otherUser = (Credential) object;
        return this.username.equals(otherUser.getUsername()) && this.password.equals(otherUser.getPassword());
    }
}
