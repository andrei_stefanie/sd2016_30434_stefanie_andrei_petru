package com.bank.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * User: Stefa
 * Date: 02-Apr-16
 * Time: 11:51
 */
@RestController
public class AuthenticationController
{
    @RequestMapping(value = "/api/authenticate/admin", method = RequestMethod.POST)
    public synchronized boolean attemptLoginAdmin(@RequestBody Map<String, String> credentials)
    {
        try
        {
            AdminController controller = new AdminController();
            Credential credential = new Credential(credentials.get("username"), credentials.get("password"), 0);
            return credential.equals(controller.getAdminCredentials(credentials.get("username")));
        } catch (NullPointerException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    @RequestMapping(value = "/api/authenticate/employee", method = RequestMethod.POST)
    public synchronized int attemptLoginEmployee(@RequestBody Map<String, String> credentials)
    {
        try
        {
            EmployeeController controller = new EmployeeController();
            Credential credential = new Credential(credentials.get("username"), credentials.get("password"), 0);
            Credential dbUser = controller.getEmployeeCredentials(credentials.get("username"));
            if(credential.equals(dbUser))
            {
                return dbUser.getId();
            }
        } catch (NullPointerException e)
        {
            e.printStackTrace();
        }
        return -1;
    }
}
