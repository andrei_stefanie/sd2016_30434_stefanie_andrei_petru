package com.bank.controller;

import com.bank.dao.AccountEntity;
import com.bank.dao.LogBillEntity;
import org.hibernate.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bank.Session.getSession;

/**
 * User: Stefa
 * Date: 26-Mar-16
 * Time: 19:43
 */
@RestController
public class BillController
{
    private org.hibernate.Session session = null;

    @RequestMapping(value = "/api/bill/all", method = RequestMethod.GET)
    public synchronized List<LogBillEntity> getBills()
    {
        session = getSession();

        String statement = "FROM LogBillEntity";
        Query query = session.createQuery(statement);

        @SuppressWarnings("unchecked")
        List<LogBillEntity> bills = query.list();

        session.close();

        return bills;
    }

    @RequestMapping(value = "/api/bill/distinct", method = RequestMethod.GET)
    public synchronized LogBillEntity getBill(@RequestParam(value = "billNumber") int billNumber)
    {
        session = getSession();

        String statement = "SELECT idlogBill FROM LogBillEntity WHERE billNr = :billNumber";
        Query query = session.createQuery(statement).setString("billNumber", String.valueOf(billNumber));
        LogBillEntity bill = (LogBillEntity) query.list().get(0);

        session.close();

        return bill;
    }

    @RequestMapping(value = "/api/bill/pay", method = RequestMethod.POST)
    public synchronized ResponseEntity.BodyBuilder payBill(@RequestBody Map<String, String> map, @RequestParam(value = "employeeID") int employeeID)
    {
        session = getSession();

        AccountEntity account = (AccountEntity) session.load(AccountEntity.class, Integer.parseInt(map.get("accountID")));
        int amount = Integer.parseInt(map.get("amount"));

        if(amount <= account.getBalance())
        {
            AccountController accountController = new AccountController();
            Map<String, String> accountMap = new HashMap<>();
            accountMap.put("accountID", String.valueOf(account.getIdaccount()));
            int newBalance = account.getBalance() - amount;
            accountMap.put("balance", String.valueOf(newBalance));
            accountController.updateAccount(accountMap, employeeID);

            LogBillEntity bill = new LogBillEntity();
            bill.setAccount(account);
            bill.setAmount(amount);
            bill.setBillNr(Integer.parseInt(map.get("billNumber")));
            bill.setPayedAt(new Timestamp(System.currentTimeMillis()));

            session.beginTransaction();
            session.save(bill);
            session.getTransaction().commit();
            session.close();

            return ResponseEntity.status(HttpStatus.OK);
        }

        session.close();
        return ResponseEntity.status(HttpStatus.PAYLOAD_TOO_LARGE);
    }

//    public static void main(final String[] args) throws Exception
//    {
//        BillController controller = new BillController();
//
//        Map<String, String> map = new HashMap<>();
//        map.put("accountID", "4");
//        map.put("amount", "100");
//        map.put("billNumber", "123");
//
//        controller.payBill(map);
//    }
}
