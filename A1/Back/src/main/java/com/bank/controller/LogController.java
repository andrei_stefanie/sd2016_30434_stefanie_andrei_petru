package com.bank.controller;

import com.bank.dao.EmployeeEntity;
import com.bank.dao.LogEmployeeEntity;
import org.hibernate.Query;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import static com.bank.Session.getSession;

/**
 * User: Stefa
 * Date: 27-Mar-16
 * Time: 13:35
 */
@RestController
public class LogController
{
    private org.hibernate.Session session = null;

    @RequestMapping(value = "/api/log", method = RequestMethod.GET)
    public synchronized List<LogEmployeeEntity> getLog(@RequestParam(value = "employeeID") int employeeID,
                                                       @RequestParam(value = "since") String since, @RequestParam(value = "till") String till)
    {
        session = getSession();

        Timestamp from = Timestamp.valueOf(since + " 00:00:00");
        Timestamp to = Timestamp.valueOf(till + " 00:00:00");

        String statement = "FROM LogEmployeeEntity AS l WHERE l.date BETWEEN :from AND :to AND idEmployee = :employeeID";
        Query query = session.createQuery(statement).setParameter("from", from).setParameter("to", to).setParameter("employeeID", employeeID);

        @SuppressWarnings("unchecked")
        List<LogEmployeeEntity> logs = query.list();

        session.close();

        return logs;
    }

    synchronized void addLog(Map<String, String> logMap)
    {
        session = getSession();

        LogEmployeeEntity log = new LogEmployeeEntity();
        EmployeeEntity employee = (EmployeeEntity) session.load(EmployeeEntity.class, Integer.parseInt(logMap.get("employeeID")));
        log.setEmployee(employee);
        log.setDate(new Timestamp(System.currentTimeMillis()));
        log.setDescription(logMap.get("description"));

        session.beginTransaction();
        session.save(log);
        session.getTransaction().commit();
        session.close();
    }

//    public static void main(final String[] args) throws Exception
//    {
//        LogController controller = new LogController();
//        List<LogEmployeeEntity> log = controller.getLog(2, "2015-01-01 00:00:00", "2017-01-01 00:00:00");
//    }
}
