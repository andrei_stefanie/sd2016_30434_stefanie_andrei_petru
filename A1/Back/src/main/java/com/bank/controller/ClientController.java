package com.bank.controller;

import com.bank.dao.ClientEntity;
import org.hibernate.Query;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bank.Session.getSession;

/**
 * User: Stefa
 * Date: 25-Mar-16
 * Time: 19:50
 */
@RestController
public class ClientController
{
    private org.hibernate.Session session = null;
    private LogController logger = new LogController();

    @RequestMapping(value = "/api/client/all", method = RequestMethod.GET)
    public synchronized List<ClientEntity> getAllClients(@RequestParam(value = "employeeID") int employeeID)
    {
        session = getSession();
        //logger = new LogController();

        String statement = "FROM ClientEntity ";
        Query query = session.createQuery(statement);

        Map<String, String> logMap = new HashMap<>();
        logMap.put("employeeID", String.valueOf(employeeID));
        logMap.put("description", "Get all clients");
        logger.addLog(logMap);

        @SuppressWarnings("unchecked")
        List<ClientEntity> clients = query.list();

        session.close();

        return clients;
    }

    @RequestMapping(value = "/api/client/distinct", method = RequestMethod.GET)
    public synchronized ClientEntity getClient(@RequestParam(value = "cnp") String cnp, @RequestParam(value = "employeeID") int employeeID)
    {
        session = getSession();
        //logger = new LogController();

        String statement = "FROM ClientEntity WHERE cnp = :cnp";
        Query query = session.createQuery(statement).setString("cnp", cnp);

        Map<String, String> logMap = new HashMap<>();
        logMap.put("employeeID", String.valueOf(employeeID));
        logMap.put("description", "Get client with cnp " + cnp);
        logger.addLog(logMap);

        ClientEntity client = (ClientEntity) query.list().get(0);

        session.close();

        return client;
    }

    @RequestMapping(value = "/api/client/new", method = RequestMethod.POST)
    public synchronized void addClient(@RequestBody Map<String, String> map, @RequestParam(value = "employeeID") int employeeID)
    {
        session = getSession();
        //logger = new LogController();

        ClientEntity client = new ClientEntity();
        client.setName(map.get("name"));
        client.setCnp(map.get("cnp"));
        client.setAddress(map.get("address"));

        Map<String, String> logMap = new HashMap<>();
        logMap.put("employeeID", String.valueOf(employeeID));
        logMap.put("description", "Added client with cnp " + map.get("cnp"));
        logger.addLog(logMap);

        session.beginTransaction();
        session.save(client);
        session.getTransaction().commit();
        session.close();
    }

    @RequestMapping(value = "/api/client/update", method = RequestMethod.PUT)
    public synchronized void updateClient(@RequestBody Map<String, String> map, @RequestParam(value = "employeeID") int employeeID)
    {
        session = getSession();

        ClientEntity client = (ClientEntity) session.load(ClientEntity.class, Integer.parseInt(map.get("clientID")));
        client.setAddress(map.get("address"));
        client.setName(map.get("name"));

        Map<String, String> logMap = new HashMap<>();
        logMap.put("employeeID", String.valueOf(employeeID));
        logMap.put("description", "Update client with cnp " + client.getCnp());
        logger.addLog(logMap);

        session.beginTransaction();
        session.update(client);
        session.getTransaction().commit();
        session.close();
    }

    @RequestMapping(value = "/api/client/remove", method = RequestMethod.DELETE)
    public synchronized void removeClient(@RequestParam(value = "cnp") String cnp, @RequestParam(value = "employeeID") int employeeID)
    {
        session = getSession();

        String statement = "SELECT idclient FROM ClientEntity WHERE cnp = :cnp";
        Query query = session.createQuery(statement).setString("cnp", cnp);
        int id = (int) query.list().get(0);
        ClientEntity client = (ClientEntity) session.load(ClientEntity.class, id);

        Map<String, String> logMap = new HashMap<>();
        logMap.put("employeeID", String.valueOf(employeeID));
        logMap.put("description", "Delete client with cnp " + client.getCnp());
        logger.addLog(logMap);

        session.beginTransaction();
        session.delete(client);
        session.getTransaction().commit();
        session.close();
    }

//    public static void main(final String[] args) throws Exception
//    {
//        ClientController controller = new ClientController();
//        //Map<String, String> map = new HashMap<>();
//        //map.put("cnp", "2222222222222");
//        //map.put("name", "test2");
//        //map.put("address", "addressNEW");
//        //map.put("clientID", "2");
//        //controller.removeClient("2222222222222");
//        System.out.println(controller.getClient("1111111111111", 2).getName());
//    }
}
