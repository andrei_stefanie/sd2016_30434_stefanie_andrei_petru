package com.bank.controller;

import com.bank.dao.AdminEntity;
import org.hibernate.Query;

import static com.bank.Session.getSession;

/**
 * User: Stefa
 * Date: 02-Apr-16
 * Time: 12:04
 */
class AdminController
{
    private org.hibernate.Session session = null;

    Credential getAdminCredentials(String username)
    {
        session = getSession();
        String statement = "FROM AdminEntity WHERE username = :username";
        Query query = session.createQuery(statement).setParameter("username", username);
        AdminEntity admin = (AdminEntity) query.list().get(0);
        return new Credential(admin.getUsername(), admin.getPassword(), 0);
    }

}
