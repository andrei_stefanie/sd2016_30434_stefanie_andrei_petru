package com.bank.controller;

import com.bank.dao.AccountEntity;
import com.bank.dao.ClientEntity;
import org.hibernate.Query;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bank.Session.getSession;

/**
 * User: Stefa
 * Date: 25-Mar-16
 * Time: 19:20
 */
@RestController
public class AccountController
{
    private org.hibernate.Session session = null;
    private LogController logger = new LogController();

    @RequestMapping(value = "/api/account/all", method = RequestMethod.GET)
    public List<AccountEntity> getAllAccounts()
    {
        session = getSession();

        String statement = "FROM AccountEntity";
        Query query = session.createQuery(statement);

        @SuppressWarnings("unchecked")
        List<AccountEntity> accounts = query.list();

        session.close();

        return accounts;
    }

    @RequestMapping(value = "/api/account/client", method = RequestMethod.GET)
    public synchronized List<AccountEntity> getClientAccounts(@RequestParam(value = "cnp") String cnp)
    {
        session = getSession();

        String statement = "SELECT a FROM AccountEntity AS a JOIN a.owner AS c WHERE c.cnp = :cnp";
        Query query = session.createQuery(statement).setString("cnp", cnp);

        @SuppressWarnings("unchecked")
        List<AccountEntity> accounts = query.list();

        session.close();

        return accounts;
    }

    @RequestMapping(value = "/api/account/new", method = RequestMethod.POST)
    public synchronized void addAccount(@RequestBody Map<String, String> map, @RequestParam(value = "employeeID") int employeeID)
    {
        session = getSession();

        AccountEntity newAccount = new AccountEntity();
        newAccount.setCreationDate(new Timestamp(System.currentTimeMillis()));
        newAccount.setType(map.get("type"));
        newAccount.setOwner((ClientEntity) session.load(ClientEntity.class, Integer.parseInt(map.get("ownerID"))));
        newAccount.setBalance(0);

        Map<String, String> logMap = new HashMap<>();
        logMap.put("employeeID", String.valueOf(employeeID));
        logMap.put("description", "Created account with for owner with ID " + map.get("ownerID"));
        logger.addLog(logMap);

        session.beginTransaction();
        session.save(newAccount);
        session.getTransaction().commit();
        session.close();
    }

    @RequestMapping(value = "/api/account/update", method = RequestMethod.PUT)
    public synchronized void updateAccount(@RequestBody Map<String, String> map, @RequestParam(value = "employeeID") int employeeID)
    {
        session = getSession();

        AccountEntity currentAccount = (AccountEntity) session.load(AccountEntity.class, Integer.parseInt(map.get("accountID")));
        currentAccount.setBalance(Integer.parseInt(map.get("balance")));

        Map<String, String> logMap = new HashMap<>();
        logMap.put("employeeID", String.valueOf(employeeID));
        logMap.put("description", "Update account " + currentAccount.getIdaccount() + " with balance " + currentAccount.getBalance());
        logger.addLog(logMap);

        session.beginTransaction();
        session.update(currentAccount);
        session.getTransaction().commit();
        session.close();
    }

    @RequestMapping(value = "/api/account/remove", method = RequestMethod.DELETE)
    public synchronized void removeAccount(@RequestParam(value = "accountID") int accountID, @RequestParam(value = "employeeID") int employeeID)
    {
        session = getSession();

        AccountEntity account = (AccountEntity) session.load(AccountEntity.class, accountID);

        Map<String, String> logMap = new HashMap<>();
        logMap.put("employeeID", String.valueOf(employeeID));
        logMap.put("description", "Delete account with id " + account.getIdaccount());
        logger.addLog(logMap);

        session.beginTransaction();
        session.delete(account);
        session.getTransaction().commit();
        session.close();
    }

    @RequestMapping(value = "/api/account/deposit", method = RequestMethod.PUT)
    public synchronized void depositAccount(@RequestBody Map<String, String> map, @RequestParam(value = "employeeID") int employeeID)
    {
        session = getSession();

        AccountEntity currentAccount = (AccountEntity) session.load(AccountEntity.class, Integer.parseInt(map.get("accountID")));
        currentAccount.setBalance(currentAccount.getBalance() + Integer.parseInt(map.get("balance")));

        Map<String, String> logMap = new HashMap<>();
        logMap.put("employeeID", String.valueOf(employeeID));
        logMap.put("description", "Deposit to account " + currentAccount.getIdaccount() + " amount " + currentAccount.getBalance());
        logger.addLog(logMap);

        session.beginTransaction();
        session.update(currentAccount);
        session.getTransaction().commit();
        session.close();
    }

//    public static void main(final String[] args) throws Exception
//    {
//        AccountController controller = new AccountController();
//
////        Map<String, String> map = new HashMap<>();
////        map.put("ownerID", "1");
////        map.put("creationDate", "2007-04-30 13:10:02");
////        map.put("type", "spending");
////        map.put("accountID", "1");
////        map.put("balance", "200");
//
//        //controller.addAccount(map);
//
//        List<AccountEntity> accounts = controller.getClientAccounts("1111111111111");
//        int balance = accounts.get(1).getBalance();
//        System.out.println(accounts.get(0).getOwner().getName());
//
//    }
}
