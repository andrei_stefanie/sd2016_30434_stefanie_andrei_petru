package com.bank.controller;

import com.bank.dao.EmployeeEntity;
import org.hibernate.Query;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import static com.bank.Session.getSession;

/**
 * User: Stefa
 * Date: 26-Mar-16
 * Time: 18:10
 */
@RestController
public class EmployeeController
{
    private org.hibernate.Session session = null;

    @RequestMapping(value = "/api/employee/all", method = RequestMethod.GET)
    public synchronized List<EmployeeEntity> getAllEmployees()
    {
        session = getSession();

        String statement = "FROM EmployeeEntity";
        Query query = session.createQuery(statement);

        @SuppressWarnings("unchecked")
        List<EmployeeEntity> employees = query.list();

        session.close();

        return employees;
    }

    @RequestMapping(value = "/api/employee/distinct", method = RequestMethod.GET)
    public synchronized EmployeeEntity getEmployee(@RequestParam(value = "employeeID") int employeeID)
    {
        session = getSession();

        EmployeeEntity employee = (EmployeeEntity) session.get(EmployeeEntity.class, employeeID);

        session.close();

        return employee;
    }

    @RequestMapping(value = "/api/employee/new", method = RequestMethod.POST)
    public synchronized void addEmployee(@RequestBody Map<String, String> map)
    {
        session = getSession();

        session.beginTransaction();
        EmployeeEntity employee = new EmployeeEntity();
        employee.setName(map.get("name"));
        employee.setEmployedSince(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
        employee.setUsername(map.get("username"));
        employee.setPassword(map.get("password"));

        session.save(employee);
        session.getTransaction().commit();
        session.close();
    }

    @RequestMapping(value = "/api/employee/update", method = RequestMethod.PUT)
    public synchronized void updateEmployee(@RequestBody Map<String, String> map)
    {
        session = getSession();

        EmployeeEntity employee = (EmployeeEntity) session.get(EmployeeEntity.class, Integer.parseInt(map.get("employeeID")));
        employee.setPassword(map.get("password"));
        employee.setName(map.get("name"));
        employee.setUsername("username");

        session.beginTransaction();
        session.update(employee);
        session.getTransaction().commit();
        session.close();
    }

    @RequestMapping(value = "/api/employee/remove", method = RequestMethod.DELETE)
    public synchronized void removeEmployee(@RequestParam(value = "employeeID") int employeeID)
    {
        session = getSession();

        EmployeeEntity employee = (EmployeeEntity) session.get(EmployeeEntity.class, employeeID);
        session.beginTransaction();
        session.delete(employee);
        session.getTransaction().commit();
        session.close();
    }

    Credential getEmployeeCredentials(String username)
    {
        session = getSession();
        String statement = "FROM EmployeeEntity WHERE username = :username";
        Query query = session.createQuery(statement).setParameter("username", username);
        EmployeeEntity employee = (EmployeeEntity) query.list().get(0);
        return new Credential(employee.getUsername(), employee.getPassword(), employee.getIdemployee());
    }

//    public static void main(final String[] args) throws Exception
//    {
//        EmployeeController controller = new EmployeeController();
//
//        Map<String, String> map = new HashMap<>();
//        //map.put("employeeID", "1");
//        map.put("name", "NewTest");
//        map.put("username", "utest");
//        map.put("password", "test");
//
//        controller.addEmployee(map);
//        //System.out.println(controller.getEmployee(1).getName());
//
//        //controller.removeEmployee(1);
//    }
}
