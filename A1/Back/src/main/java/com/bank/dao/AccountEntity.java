package com.bank.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * User: Stefa
 * Date: 24-Mar-16
 * Time: 16:34
 */
@Entity
@Table(name = "account", schema = "bank")
public class AccountEntity
{
    private int idaccount;
    private String type;
    private Integer balance;
    private Timestamp creationDate;
    private int idowner;
    @JsonIgnore
    private ClientEntity owner;

    @Id
    @Column(name = "idaccount", nullable = false)
    public int getIdaccount()
    {
        return idaccount;
    }

    public void setIdaccount(int idaccount)
    {
        this.idaccount = idaccount;
    }

    @Basic
    @Column(name = "type", nullable = false, length = 20)
    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    @Basic
    @Column(name = "balance", nullable = true)
    public Integer getBalance()
    {
        return balance;
    }

    public void setBalance(Integer balance)
    {
        this.balance = balance;
    }

    @Basic
    @Column(name = "creation_date", nullable = true)
    public Timestamp getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate)
    {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountEntity that = (AccountEntity) o;

        if (idaccount != that.idaccount) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (balance != null ? !balance.equals(that.balance) : that.balance != null) return false;
        if (creationDate != null ? !creationDate.equals(that.creationDate) : that.creationDate != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = idaccount;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (balance != null ? balance.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "idowner")
    public ClientEntity getOwner()
    {
        return owner;
    }

    public void setOwner(ClientEntity owner)
    {
        this.owner = owner;
        this.idowner = owner.getIdclient();
    }

    public int getIdowner()
    {
        return idowner;
    }

    public void setIdowner(int idowner)
    {
        this.idowner = idowner;
    }
}
