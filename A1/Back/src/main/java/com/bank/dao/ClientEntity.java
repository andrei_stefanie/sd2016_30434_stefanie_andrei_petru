package com.bank.dao;

import javax.persistence.*;

/**
 * User: Stefa
 * Date: 24-Mar-16
 * Time: 16:34
 */
@Entity
@Table(name = "client", schema = "bank")
public class ClientEntity
{
    private int idclient;
    private String name;
    private String cnp;
    private String address;

    @Id
    @Column(name = "idclient", nullable = false)
    public int getIdclient()
    {
        return idclient;
    }

    public void setIdclient(int idclient)
    {
        this.idclient = idclient;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 45)
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Basic
    @Column(name = "cnp", nullable = false, length = 13)
    public String getCnp()
    {
        return cnp;
    }

    public void setCnp(String cnp)
    {
        this.cnp = cnp;
    }

    @Basic
    @Column(name = "address", nullable = true, length = 100)
    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClientEntity that = (ClientEntity) o;

        if (idclient != that.idclient) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (cnp != null ? !cnp.equals(that.cnp) : that.cnp != null) return false;
        if (address != null ? !address.equals(that.address) : that.address != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = idclient;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (cnp != null ? cnp.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }
}
