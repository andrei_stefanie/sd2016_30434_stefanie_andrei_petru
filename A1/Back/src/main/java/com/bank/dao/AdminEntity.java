package com.bank.dao;

import javax.persistence.*;

/**
 * User: Stefa
 * Date: 24-Mar-16
 * Time: 16:34
 */
@Entity
@Table(name = "admin", schema = "bank", catalog = "")
public class AdminEntity
{
    private int idadmin;
    private String username;
    private String password;

    @Id
    @Column(name = "idadmin", nullable = false)
    public int getIdadmin()
    {
        return idadmin;
    }

    public void setIdadmin(int idadmin)
    {
        this.idadmin = idadmin;
    }

    @Basic
    @Column(name = "username", nullable = true, length = 45)
    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    @Basic
    @Column(name = "password", nullable = true, length = 45)
    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdminEntity that = (AdminEntity) o;

        if (idadmin != that.idadmin) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = idadmin;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
