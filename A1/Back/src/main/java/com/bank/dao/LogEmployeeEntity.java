package com.bank.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * User: Stefa
 * Date: 24-Mar-16
 * Time: 16:35
 */
@Entity
@Table(name = "log_employee", schema = "bank", catalog = "")
public class LogEmployeeEntity
{
    private int idlog;
    private Timestamp date;
    private String description;
    private int idEmployee;
    @JsonIgnore
    private EmployeeEntity employee;

    @Id
    @Column(name = "idlog", nullable = false)
    public int getIdlog()
    {
        return idlog;
    }

    public void setIdlog(int idlog)
    {
        this.idlog = idlog;
    }

    @Basic
    @Column(name = "date", nullable = true)
    public Timestamp getDate()
    {
        return date;
    }

    public void setDate(Timestamp date)
    {
        this.date = date;
    }

    @Basic
    @Column(name = "description", length = -1)
    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @ManyToOne @JoinColumn(name = "idemployee")
    public EmployeeEntity getEmployee()
    {
        return employee;
    }

    public void setEmployee(EmployeeEntity employee)
    {
        this.employee = employee;
        this.idEmployee = employee.getIdemployee();
    }

    public int getIdEmployee()
    {
        return idEmployee;
    }

    public void setIdEmployee(int idEmployee)
    {
        this.idEmployee = idEmployee;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogEmployeeEntity that = (LogEmployeeEntity) o;

        if (idlog != that.idlog) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = idlog;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }


}
