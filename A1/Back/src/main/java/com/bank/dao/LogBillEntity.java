package com.bank.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * User: Stefa
 * Date: 24-Mar-16
 * Time: 16:35
 */
@Entity
@Table(name = "log_bill", schema = "bank")
public class LogBillEntity
{
    private int idlogBill;
    private int billNr;
    private int amount;
    private Timestamp payedAt;
    private int idAccount;
    @JsonIgnore
    private AccountEntity account;

    @Id
    @Column(name = "idlog_bill", nullable = false)
    public int getIdlogBill()
    {
        return idlogBill;
    }

    public void setIdlogBill(int idlogBill)
    {
        this.idlogBill = idlogBill;
    }

    @Basic
    @Column(name = "bill_nr", nullable = false)
    public int getBillNr()
    {
        return billNr;
    }

    public void setBillNr(int billNr)
    {
        this.billNr = billNr;
    }

    @Basic
    @Column(name = "amount", nullable = false)
    public int getAmount()
    {
        return amount;
    }

    public void setAmount(int amount)
    {
        this.amount = amount;
    }

    @Basic
    @Column(name = "date")
    public Timestamp getPayedAt()
    {
        return payedAt;
    }

    public void setPayedAt(Timestamp payedAt)
    {
        this.payedAt = payedAt;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogBillEntity that = (LogBillEntity) o;

        if (idlogBill != that.idlogBill) return false;
        if (billNr != that.billNr) return false;
        if (amount != that.amount) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = idlogBill;
        result = 31 * result + billNr;
        result = 31 * result + amount;
        return result;
    }

    @ManyToOne @JoinColumn(name = "idaccount")
    public AccountEntity getAccount()
    {
        return account;
    }

    public void setAccount(AccountEntity account)
    {
        this.account = account;
        this.idAccount = account.getIdaccount();
    }

    public int getIdAccount()
    {
        return idAccount;
    }

    public void setIdAccount(int idAccount)
    {
        this.idAccount = idAccount;
    }
}
