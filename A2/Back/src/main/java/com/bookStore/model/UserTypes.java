package com.bookStore.model;

/**
 * User: Stefa
 * Date: 23-Apr-16
 * Time: 17:40
 */
public enum UserTypes
{
    ADMIN,
    EMPLOYEE
}
