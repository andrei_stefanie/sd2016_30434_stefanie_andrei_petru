package com.bookStore.model;

/**
 * User: Stefa
 * Date: 02-Apr-16
 * Time: 11:59
 */
public class Credential
{
    private String username, password, type;

    public Credential(String username, String password, String type)
    {
        this.username = username;
        this.password = password;
        this.type = type;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getType()
    {
        return type;
    }

    void setType(String type)
    {
        this.type = type;
    }

    public boolean equals(Object object)
    {
        if (this == object) return true;
        if (!(object instanceof Credential)) return false;

        Credential otherUser = (Credential) object;
        return this.username.equals(otherUser.getUsername()) && this.password.equals(otherUser.getPassword()) && this.type.equals(otherUser.getType());
    }
}
