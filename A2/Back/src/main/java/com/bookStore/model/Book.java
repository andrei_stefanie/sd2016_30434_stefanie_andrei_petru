package com.bookStore.model;

/**
 * User: Stefa
 * Date: 23-Apr-16
 * Time: 17:42
 */
public class Book
{
    private String title;
    private String author;
    private String genre;
    private int year;
    private float price;
    private int stock;

    public Book()
    {
    }

    public Book(String title, String author, String genre, int year, float price, int stock)
    {
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.price = price;
        this.year = year;
        this.stock = stock;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public String getGenre()
    {
        return genre;
    }

    public void setGenre(String genre)
    {
        this.genre = genre;
    }

    public int getYear()
    {
        return year;
    }

    public void setYear(int year)
    {
        this.year = year;
    }

    public float getPrice()
    {
        return price;
    }

    public void setPrice(float price)
    {
        this.price = price;
    }

    public int getStock()
    {
        return stock;
    }

    public void setStock(int stock)
    {
        this.stock = stock;
    }
}
