package com.bookStore.dao;

import au.com.bytecode.opencsv.CSVWriter;
import com.bookStore.model.Book;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * User: Stefa
 * Date: 23-Apr-16
 * Time: 17:47
 */
public class BookDAO
{
    private static BookDAO bookDAO = new BookDAO();
    private final String path = "../../../books.xml";
    private final String savePath = "../webapps/BookStore/WEB-INF/classes/books.xml";
    private final String outOfStockPDF = "../webapps/BookStore/WEB-INF/classes/books.pdf";
    private final String outOfStockCSV = "../webapps/BookStore/WEB-INF/classes/books.csv";

    private BookDAO() {}

    public static BookDAO getInstance()
    {
        return bookDAO;
    }

    public ArrayList<Book> getBooksByAttribute(String attribute, String value)
    {
        ArrayList<Book> books = new ArrayList<>();

        try
        {
            org.w3c.dom.Document document = this.getDocument();

            NodeList rootNodes = document.getElementsByTagName("books");
            Element root = (Element) rootNodes.item(0);

            NodeList booksList = root.getElementsByTagName("book");
            for(int i = 0; i < booksList.getLength(); i++)
            {
                Element bookXML = (Element) booksList.item(i);
                if(bookXML.getElementsByTagName(attribute).item(0).getTextContent().toUpperCase().equals(value.toUpperCase()))
                {
                    String title = bookXML.getElementsByTagName("title").item(0).getTextContent();
                    String author = bookXML.getElementsByTagName("author").item(0).getTextContent();
                    String year = bookXML.getElementsByTagName("year").item(0).getTextContent();
                    String genre = bookXML.getElementsByTagName("genre").item(0).getTextContent();
                    String price = bookXML.getElementsByTagName("price").item(0).getTextContent();
                    String stock = bookXML.getElementsByTagName("stock").item(0).getTextContent();
                    books.add(new Book(title, author, genre, Integer.parseInt(year), Float.parseFloat(price), Integer.parseInt(stock)));
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e)
        {
            e.printStackTrace();
        }

        return books;
    }

    public ArrayList<Book> getBooks()
    {
        ArrayList<Book> books = new ArrayList<>();

        try
        {
            Document document = this.getDocument();

            NodeList rootNodes = document.getElementsByTagName("books");
            Element root = (Element) rootNodes.item(0);

            NodeList booksList = root.getElementsByTagName("book");
            for(int i = 0; i < booksList.getLength(); i++)
            {
                Element bookXML = (Element) booksList.item(i);
                String title = bookXML.getElementsByTagName("title").item(0).getTextContent();
                String author = bookXML.getElementsByTagName("author").item(0).getTextContent();
                String year = bookXML.getElementsByTagName("year").item(0).getTextContent();
                String genre = bookXML.getElementsByTagName("genre").item(0).getTextContent();
                String price = bookXML.getElementsByTagName("price").item(0).getTextContent();
                String stock = bookXML.getElementsByTagName("stock").item(0).getTextContent();
                books.add(new Book(title, author, genre, Integer.parseInt(year), Float.parseFloat(price), Integer.parseInt(stock)));
            }
        } catch (ParserConfigurationException | SAXException | IOException e)
        {
            e.printStackTrace();
        }

        return books;
    }

    public boolean addBook(Book book)
    {
        try {
            Document document = this.getDocument();
            NodeList rootNodes = document.getElementsByTagName("books");
            Element root = (Element) rootNodes.item(0);

            if(this.getBooksByAttribute("title", book.getTitle()).size() == 0)
            {
                Element newBook = document.createElement("book");

                Element title = document.createElement("title");
                title.appendChild(document.createTextNode(book.getTitle()));
                newBook.appendChild(title);

                Element author = document.createElement("author");
                author.appendChild(document.createTextNode(book.getAuthor()));
                newBook.appendChild(author);

                Element genre = document.createElement("genre");
                genre.appendChild(document.createTextNode(book.getGenre()));
                newBook.appendChild(genre);

                Element year = document.createElement("year");
                year.appendChild(document.createTextNode("" + book.getYear()));
                newBook.appendChild(year);

                Element price = document.createElement("price");
                price.appendChild(document.createTextNode("" + book.getPrice()));
                newBook.appendChild(price);

                Element stock = document.createElement("stock");
                stock.appendChild(document.createTextNode("" + book.getStock()));
                newBook.appendChild(stock);

                root.appendChild(newBook);

                this.saveFile(document);

                return true;
            }

            NodeList booksList = root.getElementsByTagName("book");
            for(int i = 0; i < booksList.getLength(); i++)
            {
                Element bookXML = (Element) booksList.item(i);
                if(bookXML.getElementsByTagName("title").item(0).getTextContent().toUpperCase().equals(book.getTitle().toUpperCase()))
                {
                    Element oldStock = (Element) bookXML.getElementsByTagName("stock").item(0);
                    int newStockValue = Integer.parseInt(oldStock.getTextContent()) + book.getStock();
                    Element newStock = document.createElement("stock");
                    newStock.appendChild(document.createTextNode("" + newStockValue));
                    bookXML.replaceChild(newStock, oldStock);

                    this.saveFile(document);

                    return true;
                }
            }
        } catch (ParserConfigurationException | IOException | SAXException | TransformerException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public boolean updateBook(Book book)
    {
        try
        {
            Document document = this.getDocument();
            NodeList rootNodes = document.getElementsByTagName("books");
            Element root = (Element) rootNodes.item(0);

            NodeList booksList = root.getElementsByTagName("book");
            for (int i = 0; i < booksList.getLength(); i++)
            {
                Element bookXML = (Element) booksList.item(i);
                if(bookXML.getElementsByTagName("title").item(0).getTextContent().toUpperCase().equals(book.getTitle().toUpperCase()))
                {
                    Element newBook = document.createElement("book");

                    Element bookTitle = document.createElement("title");
                    bookTitle.appendChild(document.createTextNode(book.getTitle()));
                    newBook.appendChild(bookTitle);

                    Element bookAuthor = document.createElement("author");
                    bookAuthor.appendChild(document.createTextNode(book.getAuthor()));
                    newBook.appendChild(bookAuthor);

                    Element bookGenre = document.createElement("genre");
                    bookGenre.appendChild(document.createTextNode(book.getGenre()));
                    newBook.appendChild(bookGenre);

                    Element bookYear = document.createElement("year");
                    bookYear.appendChild(document.createTextNode("" + book.getYear()));
                    newBook.appendChild(bookYear);

                    Element bookPrice = document.createElement("price");
                    bookPrice.appendChild(document.createTextNode("" + book.getPrice()));
                    newBook.appendChild(bookPrice);

                    Element bookStock = document.createElement("stock");
                    bookStock.appendChild(document.createTextNode("" + book.getStock()));
                    newBook.appendChild(bookStock);

                    root.replaceChild(newBook, bookXML);

                    this.saveFile(document);

                    return true;
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public boolean removeBook(Book book)
    {
        Book toRemove;
        if((toRemove = this.getBooksByAttribute("title", book.getTitle()).get(0)) != null)
        {
            if(toRemove.getStock() >= book.getStock())
            {
                book.setStock((-1) * book.getStock());
                this.addBook(book);

                return true;
            }
        }

        return false;
    }

    public boolean deleteBook(Book book)
    {
        try
        {
            Document document = this.getDocument();
            NodeList rootNodes = document.getElementsByTagName("books");
            Element root = (Element) rootNodes.item(0);

            NodeList booksList = root.getElementsByTagName("book");
            for(int i = 0; i < booksList.getLength(); i++)
            {
                Element bookXML = (Element) booksList.item(i);
                if(bookXML.getElementsByTagName("title").item(0).getTextContent().toUpperCase().equals(book.getTitle().toUpperCase()))
                {
                    root.removeChild(bookXML);

                    this.saveFile(document);

                    return true;
                }
            }
        } catch (IOException | SAXException | ParserConfigurationException | TransformerException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    private Document getDocument() throws IOException, SAXException, ParserConfigurationException
    {
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = documentBuilder.parse(UserDAO.class.getResourceAsStream(path));
        document.normalize();

        return document;
    }

    private void saveFile(Document document) throws TransformerException
    {
        DOMSource source = new DOMSource(document);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        StreamResult result = new StreamResult(savePath);
        transformer.transform(source, result);
    }

    public String generatePDF()
    {
        ArrayList<Book> outOfStock = this.getBooksByAttribute("stock", "0");
        com.itextpdf.text.Document document = new com.itextpdf.text.Document();
        try
        {
            @SuppressWarnings("unused")
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(outOfStockPDF));
            document.open();

            if(outOfStock.size() > 0)
            {
                PdfPTable table = new PdfPTable(4);
                for (Book book : outOfStock)
                {
                    table.addCell(book.getTitle());
                    table.addCell(book.getAuthor());
                    table.addCell(book.getGenre());
                    table.addCell("" + book.getPrice());
                }

                document.add(table);
            }
        } catch (DocumentException | FileNotFoundException e)
        {
            e.printStackTrace();
        }

        document.close();

        return outOfStockPDF;
    }

    public String generateCSV()
    {
        CSVWriter writer;
        ArrayList<Book> outOfStock = this.getBooksByAttribute("stock", "0");
        try
        {
            writer = new CSVWriter(new FileWriter(outOfStockCSV), '\t');
            for (Book book : outOfStock)
            {
                String bookString = book.getTitle() + "#" + book.getAuthor() + "#" + book.getGenre() + "#" + book.getPrice();
                String[] bookDetails = bookString.split("#");
                writer.writeNext(bookDetails);
            }
            writer.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        return outOfStockCSV;
    }

//    public static void main(final String[] args)
//    {
//        BookDAO bookDAO = BookDAO.getInstance();
//        bookDAO.generatePDF();
//    }
}
