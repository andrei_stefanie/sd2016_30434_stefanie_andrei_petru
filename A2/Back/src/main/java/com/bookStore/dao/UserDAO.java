package com.bookStore.dao;

import com.bookStore.model.User;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.util.ArrayList;

/**
 * User: Stefa
 * Date: 23-Apr-16
 * Time: 17:47
 */
public class UserDAO
{
    private static UserDAO userDAO = new UserDAO();
    private final String path = "../../../users.xml";
    private final String savePath = "../webapps/BookStore/WEB-INF/classes/users.xml";

    private UserDAO() {}

    public static UserDAO getInstance()
    {
        return userDAO;
    }

    public User getUser(String username)
    {
        User user = null;

        try
        {
            Document document = this.getDocument();

            NodeList rootNodes = document.getElementsByTagName("users");
            Element root = (Element) rootNodes.item(0);

            NodeList usersList = root.getElementsByTagName("user");
            for(int i = 0; i < usersList.getLength(); i++)
            {
                Element userXML = (Element) usersList.item(i);
                if(userXML.getElementsByTagName("username").item(0).getTextContent().equals(username))
                {
                    String password = userXML.getElementsByTagName("password").item(0).getTextContent();
                    String type = userXML.getAttribute("type");
                    user = new User(type, username, password);
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e)
        {
            e.printStackTrace();
        }

        return user;
    }

    public ArrayList<User> getUsers()
    {
        ArrayList<User> users = new ArrayList<>();

        try
        {
            Document document = this.getDocument();

            NodeList rootNodes = document.getElementsByTagName("users");
            Element root = (Element) rootNodes.item(0);

            NodeList usersList = root.getElementsByTagName("user");
            for(int i = 0; i < usersList.getLength(); i++)
            {
                Element userXML = (Element) usersList.item(i);
                String type = userXML.getAttribute("type");
                String username = userXML.getElementsByTagName("username").item(0).getTextContent();
                String password = userXML.getElementsByTagName("password").item(0).getTextContent();
                User user = new User(type, username, password);

                users.add(user);
            }
        } catch (IOException | SAXException | ParserConfigurationException e)
        {
            e.printStackTrace();
        }

        return users;
    }

    public boolean addUser(User user)
    {
        try
        {
            Document document = this.getDocument();
            NodeList rootNodes = document.getElementsByTagName("users");
            Element root = (Element) rootNodes.item(0);

            if(this.getUser(user.getUsername()) == null)
            {
                Element newUser = document.createElement("user");
                newUser.setAttribute("type", user.getType());
                Element userName = document.createElement("username");
                userName.appendChild(document.createTextNode(user.getUsername()));
                newUser.appendChild(userName);
                Element password = document.createElement("password");
                password.appendChild(document.createTextNode(user.getPassword()));
                newUser.appendChild(password);

                root.appendChild(newUser);

                this.saveFile(document);

                return true;
            }
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public boolean updateUser(User user)
    {
        try
        {
            Document document = this.getDocument();
            NodeList rootNodes = document.getElementsByTagName("users");
            Element root = (Element) rootNodes.item(0);

            NodeList usersList = root.getElementsByTagName("user");
            for (int i = 0; i < usersList.getLength(); i++)
            {
                Element userXML = (Element) usersList.item(i);
                if(userXML.getElementsByTagName("username").item(0).getTextContent().equals(user.getUsername()))
                {
                    Element newUser = document.createElement("user");
                    newUser.setAttribute("type", user.getType());
                    Element userName = document.createElement("username");
                    userName.appendChild(document.createTextNode(user.getUsername()));
                    newUser.appendChild(userName);
                    Element password = document.createElement("password");
                    password.appendChild(document.createTextNode(user.getPassword()));
                    newUser.appendChild(password);

                    root.replaceChild(newUser, userXML);

                    this.saveFile(document);

                    return true;
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public boolean removeUser(User user)
    {
        try
        {
            Document document = this.getDocument();
            NodeList rootNodes = document.getElementsByTagName("users");
            Element root = (Element) rootNodes.item(0);

            NodeList usersList = root.getElementsByTagName("user");
            for(int i = 0; i < usersList.getLength(); i++)
            {
                Element userXML = (Element) usersList.item(i);
                if(userXML.getElementsByTagName("username").item(0).getTextContent().equals(user.getUsername()))
                {
                    root.removeChild(userXML);

                    this.saveFile(document);

                    return true;
                }
            }

        } catch (IOException | SAXException | ParserConfigurationException | TransformerException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    private Document getDocument() throws IOException, SAXException, ParserConfigurationException
    {
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = documentBuilder.parse(UserDAO.class.getResourceAsStream(path));
        document.normalize();

        return document;
    }

    private void saveFile(Document document) throws TransformerException
    {
        DOMSource source = new DOMSource(document);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        StreamResult result = new StreamResult(savePath);
        transformer.transform(source, result);
    }

//    public static void main(final String[] args)
//    {
//        UserDAO userDAO = UserDAO.getInstance();
//        User user = new User("employee", "append2", "append2");
//        userDAO.updateUser(user);
//        System.out.println(userDAO.getUsers());
//
//        System.out.println("" + userDAO.getUser("utest").getPassword());
//        System.out.println(UserDAO.class.getProtectionDomain().getCodeSource().getLocation().getPath());
//    }
}
