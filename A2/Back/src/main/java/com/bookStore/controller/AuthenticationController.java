package com.bookStore.controller;

import com.bookStore.dao.UserDAO;
import com.bookStore.model.Credential;
import com.bookStore.model.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * User: Stefa
 * Date: 24-Apr-16
 * Time: 14:01
 */
@RestController
public class AuthenticationController
{
    private UserDAO userDAO = UserDAO.getInstance();

    @RequestMapping(value = "api/authenticate/admin", method = RequestMethod.POST)
    public synchronized boolean attemptLoginAdmin(@RequestBody Map<String, String> credentials)
    {
        Credential attempt = new Credential(credentials.get("username"), credentials.get("password"), "admin");
        User actualUser = userDAO.getUser(credentials.get("username"));
        Credential actual = new Credential(actualUser.getUsername(), actualUser.getPassword(), actualUser.getType());
        return actual.equals(attempt);
    }

    @RequestMapping(value = "api/authenticate/employee", method = RequestMethod.POST)
    public synchronized boolean attemptLoginEmployee(@RequestBody Map<String, String> credentials)
    {
        Credential attempt = new Credential(credentials.get("username"), credentials.get("password"), "employee");
        User actualUser = userDAO.getUser(credentials.get("username"));
        Credential actual = new Credential(actualUser.getUsername(), actualUser.getPassword(), actualUser.getType());
        return actual.equals(attempt);
    }
}
