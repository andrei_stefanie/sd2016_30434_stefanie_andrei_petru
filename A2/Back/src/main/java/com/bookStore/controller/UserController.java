package com.bookStore.controller;

import com.bookStore.dao.UserDAO;
import com.bookStore.model.User;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * User: Stefa
 * Date: 24-Apr-16
 * Time: 13:34
 */
@RestController
public class UserController
{
    private UserDAO userDAO = UserDAO.getInstance();

    @RequestMapping(value = "/api/users/all", method = RequestMethod.GET)
    public synchronized List<User> getAllEmployees()
    {
        return userDAO.getUsers();
    }

    @RequestMapping(value = "/api/users/add", method = RequestMethod.POST)
    public synchronized void addUser(@RequestBody Map<String, String> map)
    {
        User user = new User(map.get("type"), map.get("username"), map.get("password"));
        userDAO.addUser(user);
    }

    @RequestMapping(value = "/api/users/remove", method = RequestMethod.DELETE)
    public synchronized void deleteUser(@RequestParam(value = "username") String username)
    {
        User user = new User(null, username, null);
        userDAO.removeUser(user);
    }

    @RequestMapping(value = "/api/users/update", method = RequestMethod.PUT)
    public synchronized void updateUser(@RequestBody Map<String, String> map)
    {
        User user = new User(map.get("type"), map.get("username"), map.get("password"));
        userDAO.updateUser(user);
    }
}
