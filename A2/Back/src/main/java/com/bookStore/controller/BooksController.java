package com.bookStore.controller;

import com.bookStore.dao.BookDAO;
import com.bookStore.model.Book;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;

/**
 * User: Stefa
 * Date: 24-Apr-16
 * Time: 12:40
 */
@RestController
public class BooksController
{
    private BookDAO bookDAO = BookDAO.getInstance();

    @RequestMapping(value = "/api/books/all", method = RequestMethod.GET)
    public synchronized ArrayList<Book> getAllBooks()
    {
        ArrayList<Book> books = bookDAO.getBooks();
        int x = 2;
        return books;
    }

    @RequestMapping(value = "/api/books/title", method = RequestMethod.GET)
    public synchronized ArrayList<Book> getBookByTitle(@RequestParam(value = "attribute") String attribute, @RequestParam(value = "value") String value)
    {
        return bookDAO.getBooksByAttribute(attribute, value);
    }

    @RequestMapping(value = "/api/books/add", method = RequestMethod.POST)
    public synchronized void addBook(@RequestBody Map<String, String> map)
    {
        if (Integer.parseInt(map.get("stock")) > 0)
        {
            Book book = new Book();
            book.setTitle(map.get("title"));
            book.setAuthor(map.get("author"));
            book.setGenre(map.get("genre"));
            book.setPrice(Float.parseFloat(map.get("price")));
            book.setYear(Integer.parseInt(map.get("year")));
            book.setStock(Integer.parseInt(map.get("stock")));
            bookDAO.addBook(book);
        }
    }

    @RequestMapping(value = "/api/books/remove", method = RequestMethod.POST)
    public synchronized void removeBook(@RequestBody Map<String, String> map)
    {
        bookDAO.removeBook(new Book(map.get("title"), null, null, 0, 0, Integer.parseInt(map.get("stock"))));
    }

    @RequestMapping(value = "/api/books/delete", method = RequestMethod.DELETE)
    public synchronized void deleteBook(@RequestParam(value = "title") String title)
    {
        bookDAO.deleteBook(new Book(title, null, null, 0, 0, 0));
    }

    @RequestMapping(value = "/api/books/update", method = RequestMethod.PUT)
    public synchronized void updateBook(@RequestBody Map<String, String> map)
    {
        String title = map.get("title");
        String author = map.get("author");
        String genre = map.get("genre");
        String year = map.get("year");
        String price = map.get("price");
        String stock = map.get("stock");
        bookDAO.updateBook(new Book(title, author, genre, Integer.parseInt(year), Float.parseFloat(price), Integer.parseInt(stock)));
    }

    @RequestMapping(value = "/api/outOfStock/{format}", method = RequestMethod.GET)
    public synchronized void getFile(@PathVariable("format") String format, HttpServletResponse response)
    {
        try
        {
            InputStream is;
            if("pdf".equals(format.toLowerCase()))
            {
                is = new FileInputStream(bookDAO.generatePDF());
                response.setContentType("application/pdf");
                response.setHeader("Content-Disposition", "attachment; filename=\"report.pdf\"");
            } else {
                is = new FileInputStream(bookDAO.generateCSV());
                response.setContentType("application/csv");
                response.setHeader("Content-Disposition", "attachment; filename=\"report.csv\"");
            }

            org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
            is.close();
        } catch (IOException ex)
        {
            throw new RuntimeException("IOError writing file to output stream");
        }
    }
}
