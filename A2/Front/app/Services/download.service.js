(function() {
    'use strict';

    var downloadModule = angular.module('components.download', []);

    downloadModule.factory('downloadService', ['$q', '$timeout', '$window',
        function($q, $timeout, $window) {
            return {
                download: function(format) {

                    var defer = $q.defer();

                    $timeout(function() {
                        $window.location = 'http://localhost:8080/BookStore/api/outOfStock/' + format;

                    }, 1000)
                        .then(function() {
                            defer.resolve('success');
                        }, function() {
                            defer.reject('error');
                        });
                    return defer.promise;
                }
            };
        }
    ]);
})();