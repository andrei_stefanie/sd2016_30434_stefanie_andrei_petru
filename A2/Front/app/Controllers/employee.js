/**
 * Created by Stefa on 30-Mar-16.
 */

'use strict';

angular.module('bookStoreApp.employee', [])
    .controller('EmployeeCtrl', function ($scope, $q, requestFactory, $rootScope, $location, AuthenticationService)
    {
        $scope.books = [];
        $scope.selectedBook = {};
        $scope.searchValue = "";
        $scope.amountToSell = 0;

        var config = {headers: {'Content-Type': 'application/json'}};

        $scope.getBooks = function () {
            requestFactory.getAllBooks()
                .then(
                    function (response) {
                        $scope.books = response.data;
                    },
                    function(errResponse, status){
                        alert("Error while retrieving books: " + status);
                        return $q.reject(errResponse);
                    }
                );
        };

        $scope.getBooks();

        $scope.showBook = function (book) {
            $scope.selectedBook = book;
        };
        
        $scope.sellBook = function (amount) {
            if($scope.selectedBook != {}) {
                if(amount <= $scope.selectedBook.stock && amount > 0) {
                    var books = {};
                    books["title"] = $scope.selectedBook.title;
                    books["stock"] = amount;

                    requestFactory.sellBooks(books, config)
                        .then(
                            function () {
                                $scope.getBooks();
                                $scope.selectedBook.stock = $scope.selectedBook.stock - amount;
                            },
                            function () {
                                alert("Operation failed: " + status);
                            });
                }
                else {
                    alert("Invalid amount of books");
                }
            }  
            else {
                alert("Select a book from the table first");
            }
        };

        $scope.logout = function () {
            $location.path('/');
            AuthenticationService.ClearCredentials();
        }
    });