/**
 * Created by Stefa on 30-Mar-16.
 */

angular.module('bookStoreApp.manager', [])
    .controller('ManagerCtrl', function ($scope, $q, requestFactory, $location, AuthenticationService, downloadService)
    {
        $scope.inputType = "password";
        $scope.options = { 'height' : '500'};
        $scope.books = [];
        $scope.selectedBook = {};
        $scope.passwordCheckbox = {};
        $scope.booksVisible = true;
        $scope.users = [];
        $scope.selectedUser = {};
        $scope.searchValueBook = "";
        $scope.searchValueUser = "";
        $scope.reportFormat = "";
        
        var config = {headers: {'Content-Type': 'application/json'}};

        $scope.showBooksDiv = function () {
            $scope.booksVisible = true;
        };

        $scope.showUsersDiv = function () {
            $scope.booksVisible = false;
        };
        
        $scope.getBooks = function () {
            requestFactory.getAllBooks()
                .then(
                    function (response) {
                        $scope.books = response.data;
                    },
                    function(errResponse, status){
                        alert("Error while retrieving books: " + status);
                        return $q.reject(errResponse);
                    }
                );
        };

        $scope.getBooks();

        $scope.showBook = function (book) {
            $scope.selectedBook = book;
        };

        $scope.clearBook = function () {
            $scope.selectedBook = {};
        };

        $scope.insertBook = function () {
            if(!fieldsFilledIn()) {
                alert("All the fields must be filled in");
                return;
            }
            var book = {};
            book['title'] = $scope.selectedBook.title;
            book['author'] = $scope.selectedBook.author;
            book['genre'] = $scope.selectedBook.genre;
            book['year'] = $scope.selectedBook.year;
            book['price'] = $scope.selectedBook.price;
            book['stock'] = $scope.selectedBook.stock;

            requestFactory.createBook(book, config)
                .then(
                    function () {
                        $scope.getBooks();
                        $scope.clearBook();
                    },
                    function ()
                    {
                        alert("Duplicate title: " + status);
                    });
        };

        $scope.updateBook = function () {
            if(!fieldsFilledIn()) {
                alert("All the fields must be filled in");
                return;
            }
            var book = {};
            book['title'] = $scope.selectedBook.title;
            book['author'] = $scope.selectedBook.author;
            book['genre'] = $scope.selectedBook.genre;
            book['year'] = $scope.selectedBook.year;
            book['price'] = $scope.selectedBook.price;
            book['stock'] = $scope.selectedBook.stock;

            requestFactory.updateBook(book, config)
                .then(
                    function () {
                        $scope.getBooks();
                        $scope.clearBook();
                    },
                    function ()
                    {
                        alert("Update Failed: " + status);
                    });
        };

        var fieldsFilledIn = function () {
            return !!($scope.selectedBook.title !== "" && $scope.selectedBook.author !== "" && $scope.selectedBook.genre !== ""
            && $scope.selectedBook.price > 0 && $scope.selectedBook.year > 0 && $scope.selectedBook.stock >= 0);
        };

        $scope.deleteBook = function () {
            var book = {};
            book['title'] = $scope.selectedBook.title;
            
            requestFactory.deleteBook(book.title, config)
                .then(
                    function () {
                        $scope.getBooks();
                        $scope.clearBook();
                    },
                    function () {
                        alert("Delete failed" + status);
                    }
                )
        };

        $scope.showUser = function (user) {
            $scope.selectedUser = user;
        };

        $scope.clearUser = function () {
            $scope.selectedUser = {};
        };

        $scope.getUsers = function () {
            requestFactory.getAllUsers()
                .then(
                    function (response) {
                        $scope.users = response.data;
                    },
                    function(errResponse, status){
                        alert("Error while retrieving users: " + status);
                        return $q.reject(errResponse);
                    }
                );
        };

        $scope.insertUser = function () {
            if(!validUser()) {
                alert("All the fields must be filled in and the password must be at least 5 characters long");
                return;
            }

            var user = {};
            user["username"] = $scope.selectedUser.username;
            user["password"] = $scope.selectedUser.password;
            user["type"] = $scope.selectedUser.type;

            if($scope.users.indexOf($scope.selectedUser.username) === -1) {
                requestFactory.insertUser(user, config)
                    .then(
                        function () {
                            $scope.getUsers();
                            $scope.clearUser();
                        },
                        function ()
                        {
                            alert("Operation failed: " + status);
                        });
            } else {
                alert("Duplicate username: " + status);
            }
        };

        $scope.updateUser = function () {
            if(!validUser()) {
                alert("All the fields must be filled in and the password must be at least 5 characters long");
                return;
            }

            var user = {};
            user["username"] = $scope.selectedUser.username;
            user["password"] = $scope.selectedUser.password;
            user["username"] = $scope.selectedUser.type;

            requestFactory.updateUser(user, config)
                .then(
                    function () {
                        $scope.getUsers();
                        $scope.clearUser();
                    },
                    function ()
                    {
                        alert("Duplicate username: " + status);
                    });
        };

        var validUser = function () {
            return (($scope.selectedUser.username !== "") && ($scope.selectedUser.password.length > 5));
        };
        
        $scope.deleteUser = function () {
            var user = {};
            user["username"] = $scope.selectedUser.username;

            requestFactory.deleteUser(user, config)
                .then(
                    function () {
                        $scope.getUsers();
                        $scope.clearUser();
                    },
                    function ()
                    {
                        alert("Duplicate username: " + status);
                    });
        };

        $scope.hideShowPassword = function(){
            if ($scope.inputType == 'password')
                $scope.inputType = 'text';
            else
                $scope.inputType = 'password';
        };

        $scope.logout = function () {
            $location.path('/');
            AuthenticationService.ClearCredentials();
        };

        $scope.download = function(format) {
            downloadService.download(format)
                .then(function(success) {
                    console.log('success : ' + success);
                }, function(error) {
                    console.log('error : ' + error);
                });
        };
    })
    .directive( 'goClick', function ( $location ) {
        return function ( scope, element, attrs ) {
            var path;

            attrs.$observe( 'goClick', function (val) {
                path = val;
            });

            element.bind( 'click', function () {
                scope.$apply( function () {
                    $location.path( path );
                });
            });
        };
    });