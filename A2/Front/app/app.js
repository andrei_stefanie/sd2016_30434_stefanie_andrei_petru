'use strict';

angular.module('bookStoreApp', [
        'ngRoute',
        'ngCookies',
        'bookStoreApp.homepage',
        'bookStoreApp.employee',
        'bookStoreApp.manager',
        'bookStoreApp.login',
        'components.download'
    ])
    .config(['$routeProvider', function($routeProvider)
    {
        $routeProvider
            .when('/', {
                templateUrl: 'Views/homepage.html',
                controller: 'HomepageCtrl'
            })
            .when('/manager', {
                templateUrl: 'Views/manager.html',
                controller: 'ManagerCtrl'
            })
            .when('/employee', {
                templateUrl: 'Views/employee.html',
                controller: 'EmployeeCtrl'
            })
            .when('/loginManager', {
                templateUrl: 'Views/login.html',
                controller: 'LoginAdminCtrl'
            })
            .when('/loginEmployee', {
                templateUrl: 'Views/login.html',
                controller: 'LoginEmployeeCtrl'
            })
            .otherwise({redirectTo: '/'});
    }])
    .run(['$rootScope', '$location', '$cookieStore', '$http', function($rootScope, $location, $cookieStore, $http)
    {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser)
        {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
        }

        $rootScope.$on('$locationChangeStart', function ()
        {
            //redirect to homepage if not logged in and trying to access a restricted page

            var restrictedPage = $.inArray($location.path(), ['/loginManager', '/loginEmployee', '/']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if(restrictedPage && !loggedIn)
            {
                $location.path('/');
            }
            //redirect to homepage if employee tries to access manager page
            if(loggedIn && $rootScope.globals.currentUser.userType === "employee" && $location.path() === '/manager')
            {
                $location.path('/');
            }
            //skip login page if already logged in
            if(loggedIn && $rootScope.globals.currentUser.userType === "employee" && $location.path() === '/loginEmployee')
            {
                $location.path('/employee')
            }
            if(loggedIn && $rootScope.globals.currentUser.userType === "admin" && $location.path() === '/loginManager')
            {
                $location.path('/manager')
            }
        });
    }])
    .factory('requestFactory', function ($http)
    {
        var factory = {};

        factory.getAllBooks = function ()
        {
            return $http.get("http://localhost:8080/BookStore/api/books/all");
        };

        factory.sellBooks = function (books, config) {
            return $http.post("http://localhost:8080/BookStore/api/books/remove", books, config);
        };

        factory.createBook = function (book, config) {
            return $http.post("http://localhost:8080/BookStore/api/books/add", book, config);
        };

        factory.updateBook = function (book, config) {
            return $http.put("http://localhost:8080/BookStore/api/books/update", book, config);
        };

        factory.deleteBook = function (bookTitle, config) {
            return $http.delete("http://localhost:8080/BookStore/api/books/delete?title=" + bookTitle, config);
        };

        factory.getAllUsers = function ()
        {
            return $http.get("http://localhost:8080/BookStore/api/users/all");
        };

        factory.insertUser = function (user, config) {
            return $http.post("http://localhost:8080/BookStore/api/users/add", user, config);
        };

        factory.updateUser = function (user, config) {
            return $http.put("http://localhost:8080/BookStore/api/users/update", user, config);
        };

        factory.deleteUser = function (username, config) {
            return $http.delete("http://localhost:8080/BookStore/api/users/remove?username=" + username, config);
        };

        factory.generateReport = function (format) {
            return $http.get("http://localhost:8080/BookStore/api/users/remove/" + format);
        };

        return factory;
    });
