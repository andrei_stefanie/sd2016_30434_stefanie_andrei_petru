/**
 * Created by Stefa on 26-May-16.
 */


angular.module('lunarBet.lunar', [])
    .controller('LunarCtrl', function ($scope, $location, $interval, AuthenticationService, requestFactory)
    {
        $scope.showBets = true;
        $scope.showResults = false;
        $scope.searchValue = '';
        $scope.footballEvents = [];
        $scope.basketEvents = [];
        $scope.tennisEvents = [];
        $scope.selectedEvents = [];
        $scope.selectedSport = 0;
        $scope.ticketNumber = '';
        $scope.showTicketResult = false;
        $scope.ticketWon = false;
        $scope.ticketResult = '';

        $scope.currentTicket = {};
        $scope.currentTicket.odd = 1.0;
        $scope.currentTicket.bet = 0.0;
        $scope.currentTicket.potential = 0.0;
        $scope.currentTicket.selected = false;

        $scope.userTickets = [];

        $scope.go = function ( path )
        {
            $location.path( path );
        };

        $scope.displayContent = function (content) {
            if(content === 'bet') {
                $scope.showBets = true;
                $scope.showResults = false;
            } else if(content === 'result') {
                $scope.showBets = false;
                $scope.showResults = true;
            }
        };

        $scope.selectEvents = function (sport) {
            if(sport === 'football') {
                $scope.selectedSport = 0;
                $scope.selectedEvents = $scope.footballEvents;
            } else if(sport === 'basket') {
                $scope.selectedSport = 1;
                $scope.selectedEvents = $scope.basketEvents;
            } else if(sport === 'tennis') {
                $scope.selectedSport = 2;
                $scope.selectedEvents = $scope.tennisEvents;
            }
        };

        $scope.checkTicket = function () {
            $scope.showTicketResult = true;
            
        };

        $scope.logout = function () {
            $location.path('/');
            AuthenticationService.ClearCredentials();
        };

        $scope.getOffer = function (sport) {
            return requestFactory.getOffer(sport)
                .then(
                    function (response) {
                        switch(sport){
                            case "football":
                                $scope.footballEvents = response.data;
                                break;
                            case "basket":
                                $scope.basketEvents = response.data;
                                break;
                            case "tennis":
                                $scope.tennisEvents = response.data;
                        }

                        $scope.selectEvents(sport);
                    },
                    function () {
                        // alert("Could not retrieve sport events");
                    }
                )
        };

        var getAllEvents = function () {
            if($location.path() === "/lunar") {
                $scope.getOffer("basket")
                    .then(function () {
                        $scope.getOffer("tennis")
                            .then(
                                function () {
                                    $scope.getOffer("football");
                                }
                            , function () {

                            });
                    }, function () {

                    });
            }
        };

        // $interval(getAllEvents, 5000);
        getAllEvents();

        $scope.convertToDate = function (timestamp) {
            return moment(timestamp).format("DD.MM.YYYY  hh:mm");
        };
        
        $scope.addToTicket = function (odd, selected) {
            if(!selected)
            {
                $scope.currentTicket.odd *= odd;
                $scope.currentTicket.potential = $scope.currentTicket.bet * $scope.currentTicket.odd;
            }
        };
        
        $scope.makeBet = function () {
            
        };

        $scope.clearBet = function () {
            $scope.currentTicket = {};
            $scope.currentTicket.odd = 1.0;
            $scope.currentTicket.bet = 0.0;
            $scope.currentTicket.potential = 0.0;
            $scope.currentTicket.selected = false;
        };
    });
