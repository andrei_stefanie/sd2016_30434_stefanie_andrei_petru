/**
 * Created by Stefa on 30-Mar-16.
 */

'use strict';

angular.module('lunarBet.homepage', [])
    .controller('HomepageCtrl', function ($scope, $location, $interval, requestFactory)
    {
        $scope.showBets = true;
        $scope.showResults = false;
        $scope.searchValue = '';
        $scope.footballEvents = [];
        $scope.basketEvents = [];
        $scope.tennisEvents = [];
        $scope.selectedEvents = [];
        $scope.selectedSport = 0;
        $scope.ticketNumber = '';
        $scope.showTicketResult = false;
        $scope.ticketWon = false;
        $scope.ticketResult = '';

        $scope.go = function ( path )
        {
            $location.path( path );
        };

        $scope.displayContent = function (content) {
            if(content === 'bet') {
                $scope.showBets = true;
                $scope.showResults = false;
            } else if(content === 'result') {
                $scope.showBets = false;
                $scope.showResults = true;
            }
        };

        $scope.selectEvents = function (sport) {
            if(sport === 'football') {
                $scope.selectedSport = 0;
                $scope.selectedEvents = $scope.footballEvents;
            } else if(sport === 'basket') {
                $scope.selectedSport = 1;
                $scope.selectedEvents = $scope.basketEvents;
            } else if(sport === 'tennis') {
                $scope.selectedSport = 2;
                $scope.selectedEvents = $scope.tennisEvents;
            }
        };
        
        $scope.checkTicket = function () {
            $scope.showTicketResult = true;
        };
        
        $scope.getOffer = function (sport) {
            return requestFactory.getOffer(sport)
                .then(
                    function (response) {
                        switch(sport){
                            case "football":
                                $scope.footballEvents = response.data;
                                break;
                            case "basket":
                                $scope.basketEvents = response.data;
                                break;
                            case "tennis":
                                $scope.tennisEvents = response.data;
                        }

                        $scope.selectEvents(sport);
                    },
                    function () {
                        alert("Could not retrieve sport events");
                    }
                )
        };

        var getAllEvents = function () {
            if($location.path() === "/") {
                $scope.getOffer("basket")
                    .then(function () {
                        $scope.getOffer("tennis")
                            .then(
                                function () {
                                    $scope.getOffer("football");
                                }
                                , function () {

                                });
                    }, function () {

                    });
            }
        };

        $interval(getAllEvents, 10000);
        getAllEvents();

        $scope.convertToDate = function (timestamp) {
            return moment(timestamp).format("DD.MM.YYYY  hh:mm");
        };
    });