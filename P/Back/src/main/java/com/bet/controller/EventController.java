package com.bet.controller;

import com.bet.dao.EventEntity;
import com.bet.SessionDB;
import org.hibernate.Query;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.concurrent.ThreadLocalRandom;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Stefa
 * Date: 26-May-16
 * Time: 13:19
 */
@RestController
public class EventController
{
	private org.hibernate.Session session;

	@RequestMapping(value = "/events/sport", method = RequestMethod.GET)
	public List<EventEntity> getSportOffer(@RequestParam(value = "sport") String sport)
	{
		session = SessionDB.getSession();
		Timestamp now = new Timestamp(System.currentTimeMillis());
		String stm = "FROM EventEntity WHERE sport = :sport AND moment >= :moment";
		Query query = session.createQuery(stm).setString("sport", sport).setString("moment", "" + now);
		@SuppressWarnings("unchecked")
		ArrayList<EventEntity> events = (ArrayList<EventEntity>) query.list();
		session.close();
		return events;
	}

	@RequestMapping(value = "/events/generate", method = RequestMethod.GET)
	public boolean generateRandomEvents()
	{
		session = SessionDB.getSession();
		String sports[] = { "football", "basket", "tennis" };
		String events[][] = {{"Steaua", "Dinamo", "Rapid", "Chelsea", "Liverpool", "Bayern", "Real Madrid", "Athletico", "U Cluj"},
				{"AAA", "BBB", "CCC", "DDD", "EEE", "FFF", "GGG", "HHH", "III"},
				{"t1", "t2", "t3", "t4", "t5", "t6", "t7", "t8", "t9"}};
		long offset = System.currentTimeMillis();
		long end = Timestamp.valueOf("2016-06-01 00:00:00").getTime();
		long diff = end - offset + 1;;
		for(int s = 0; s < 3; s++) {
			for(int times = 0; times < 3; times++) {
				int team1 = ThreadLocalRandom.current().nextInt(0, 9);
				int team2 = team1;
				while(team2 == team1)
				{
					team2 = ThreadLocalRandom.current().nextInt(0, 9);
				}
				Timestamp rand = new Timestamp(offset + (long)(Math.random() * diff));
				double odd1 = ((double)ThreadLocalRandom.current().nextInt(105, 1500) / 100.0);
				double oddX = ((double)ThreadLocalRandom.current().nextInt(105, 1500) / 100.0);
				double odd2 = ((double)ThreadLocalRandom.current().nextInt(105, 1500) / 100.0);
				EventEntity genEvent = new EventEntity();
				genEvent.setBet1(odd1);
				genEvent.setBetX(oddX);
				genEvent.setBet2(odd2);
				genEvent.setSport(sports[s]);
				genEvent.setTeamA(events[s][team1]);
				genEvent.setTeamB(events[s][team2]);
				genEvent.setMoment(rand);
				genEvent.setTimes(0);
				session.beginTransaction();
				session.save(genEvent);
				session.getTransaction().commit();
//				session.close();
			}
		}

		session.close();

		return true;
	}
}
