package com.bet.controller;

import com.bet.SessionDB;
import com.bet.dao.ResultsEntity;
import org.hibernate.Query;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Stefa
 * Date: 26-May-16
 * Time: 15:12
 */
@RestController
public class ResultController
{
	private org.hibernate.Session session;

	@RequestMapping(value = "/result/all", method = RequestMethod.GET)
	public List<ResultsEntity> getAllResults()
	{
		session = SessionDB.getSession();
		String stm = "FROM ResultsEntity ";
		Query query = session.createQuery(stm);
		@SuppressWarnings("unchecked")
		ArrayList<ResultsEntity> results = (ArrayList<ResultsEntity>) query.list();
		return results;
	}

	@RequestMapping(value = "/result/ticket", method = RequestMethod.GET)
	public void getResultForTicket(@RequestParam(value = "ticket") int ticket)
	{

	}
}
