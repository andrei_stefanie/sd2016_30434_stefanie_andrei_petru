package com.bet.controller;

import com.bet.SessionDB;
import com.bet.dao.UserDetailsEntity;
import com.bet.dao.UserEntity;
import com.bet.model.Credential;
import org.hibernate.Query;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * User: Stefa
 * Date: 02-Apr-16
 * Time: 11:51
 */
@RestController
public class AuthenticationController
{
    private org.hibernate.Session session;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public synchronized Map<String, String> attemptLogin(@RequestBody Map<String, String> credentials)
    {
        session = SessionDB.getSession();

        try
        {
            Credential testUser = new Credential(credentials.get("username"), credentials.get("password"));

            String statement = "FROM UserEntity WHERE username = :username";
            Query query = session.createQuery(statement).setParameter("username", credentials.get("username"));
            UserEntity user = (UserEntity) query.list().get(0);
            Credential dbUser = new Credential(user.getUsername(), user.getPassword());
            if(testUser.equals(dbUser))
            {
	            Map<String, String> map = new HashMap<>(1);
	            map.put("userType", user.getType());
                map.put("userID", "" + user.getUserId());
	            session.close();
                return map;
            }
        } catch (NullPointerException e)
        {
            e.printStackTrace();
        }
        session.close();
        return null;
    }

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public int register(@RequestBody Map<String, String> credentials)
	{
		session = SessionDB.getSession();
		String stm = "FROM UserEntity WHERE username = :username";
		Query query = session.createQuery(stm).setParameter("username", credentials.get("username"));
		if(query.list().size() == 0)
		{
			UserEntity user = new UserEntity();
			user.setPassword(credentials.get("password"));
			user.setType("client");
			user.setUsername(credentials.get("username"));
			session.beginTransaction();
			session.save(user);
			session.getTransaction().commit();

			UserDetailsEntity details = new UserDetailsEntity();
			details.setBalance(10.0);
			details.setUserId(user.getUserId());
			details.setEmail(credentials.get("email"));
			session.beginTransaction();
			session.save(details);
			session.getTransaction().commit();
			return user.getUserId();
		}
		session.close();
		return 0;
	}
}
