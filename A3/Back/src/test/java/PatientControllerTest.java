import com.clinic.SessionDB;
import com.clinic.controller.PatientController;
import com.clinic.model.PatientEntity;
import org.hibernate.Query;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * User: Stefa
 * Date: 16-May-16
 * Time: 15:39
 */
public class PatientControllerTest
{
	@Test
	public void addPatient() throws Exception
	{
		Map<String, String> patientMap = new HashMap<>();
		patientMap.put("name", "testPatient");
		patientMap.put("birthDate", "946684800");
		patientMap.put("address", "test street");
		patientMap.put("cnp", "1111111111111");
		PatientController controller = new PatientController();
		controller.addPatient(patientMap);

		org.hibernate.Session session = SessionDB.getSession();
		String stm = "FROM PatientEntity WHERE name = :patientName";
		Query query = session.createQuery(stm).setParameter("patientName", "testPatient");
		PatientEntity dbPatient = (PatientEntity) query.list().get(0);

		assertEquals("1111111111111", dbPatient.getCnp());
	}

	@Test
	public void updatePatient() throws Exception
	{

	}

	@Test
	public void removePatient() throws Exception
	{

	}

	@Test
	public void getAllPatients() throws Exception
	{

	}

}