import com.clinic.SessionDB;
import com.clinic.controller.UserController;
import com.clinic.model.DoctorEntity;
import com.clinic.model.UserEntity;
import org.hibernate.Query;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * User: Stefa
 * Date: 16-May-16
 * Time: 15:14
 */
public class UserControllerTest
{
	@Test
	public void addUser() throws Exception
	{
		Map<String, String> userMap = new HashMap<>();
		userMap.put("username", "doctorUser");
		userMap.put("password", "test");
		userMap.put("type", "doctor");
		userMap.put("doctorName", "dr. test");
		UserController controller = new UserController();
		controller.addUser(userMap);

		org.hibernate.Session session = SessionDB.getSession();
		String stm = "FROM DoctorEntity WHERE doctorName = :doctorName";
		Query query = session.createQuery(stm).setString("doctorName", "dr. test");
		DoctorEntity dbUser = (DoctorEntity) query.list().get(0);
		assertEquals(dbUser.getDoctorName(), "dr. test");
	}

	@Test
	public void updateUser() throws Exception
	{
//		Map<String, String> userMap = new HashMap<>();
//		userMap.put("username", "test");
//		userMap.put("password", "test");
//		userMap.put("type", "admin");
//		userMap.put("userID", "4");
//		UserController controller = new UserController();
//		controller.updateUser(userMap);
//
//		org.hibernate.Session session = SessionDB.getSession();
//		UserEntity dbUser = (UserEntity) session.load(UserEntity.class, 4);
//		assertEquals("test", dbUser.getUsername());
	}

	@Test
	public void removePatient() throws Exception
	{

	}

	@Test
	public void getAllUsers() throws Exception
	{

	}

}