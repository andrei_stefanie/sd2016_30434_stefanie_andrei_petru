import com.clinic.controller.DoctorController;
import com.clinic.model.DoctorEntity;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * User: Stefa
 * Date: 18-May-16
 * Time: 21:20
 */
public class DoctorControllerTest
{
	@Test
	public void getDoctorsForTime() throws Exception
	{
		DoctorController controller = new DoctorController();
		List<DoctorEntity> doctors = controller.getDoctorsForTime("1463835720000");
		assertEquals(1, doctors.size());
	}

}