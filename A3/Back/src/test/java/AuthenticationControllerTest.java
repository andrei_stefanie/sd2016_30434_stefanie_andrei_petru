import com.clinic.SessionDB;
import com.clinic.controller.AuthenticationController;
import com.clinic.model.DoctorEntity;
import com.clinic.model.UserEntity;
import org.hibernate.Query;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * User: Stefa
 * Date: 21-May-16
 * Time: 21:20
 */
public class AuthenticationControllerTest
{
	@Test
	public void attemptLogin() throws Exception
	{
		AuthenticationController controller = new AuthenticationController();
		Map<String, String> credential = new HashMap<>();
		credential.put("username", "testD");
		credential.put("password", "test");

		//Map<String, String> map = controller.attemptLogin(credential);

		org.hibernate.Session session = SessionDB.getSession();
		UserEntity user = new UserEntity();
		user.setType("doctor");
		user.setPassword("test");
		user.setUsername("testD");
		user.setUserId(14);

		String stm = "FROM DoctorEntity WHERE userId = :user";
		Query query = session.createQuery(stm).setParameter("user", user.getUserId());
		DoctorEntity doc = (DoctorEntity) query.list().get(0);
		//assertEquals(doc, credential);
	}

}