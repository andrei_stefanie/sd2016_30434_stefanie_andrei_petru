import com.clinic.SessionDB;
import com.clinic.controller.ConsultController;
import com.clinic.model.Consult;
import com.clinic.model.ConsultationEntity;
import com.clinic.model.DoctorEntity;
import org.hibernate.Query;
import org.junit.Test;

import javax.print.Doc;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * User: Stefa
 * Date: 16-May-16
 * Time: 15:39
 */
public class ConsultControllerTest
{
	@Test
	public void addConsult() throws Exception
	{
//		Map<String, String> consultMap = new HashMap<>();
//		consultMap.put("doctorID", "2");
//		consultMap.put("patientID", "1");
//		consultMap.put("since", "1463404509000");
//		consultMap.put("till", "1463411123321");
//		ConsultController controller = new ConsultController();
//		controller.addConsult(consultMap);
//
//		Consult dbConsult = controller.getAllConsults().get(0);
//		assertEquals("dr. test", dbConsult.getDoctorName());
	}

	@Test
	public void updateConsult() throws Exception
	{

	}

	@Test
	public void removeConsult() throws Exception
	{

	}

	@Test
	public void getAllConsults() throws Exception
	{
		ConsultController controller = new ConsultController();
		List<Consult> dbConsults = controller.getAllConsults();
		assertEquals("dr. test", dbConsults.get(0).getDoctorName());
	}

	@Test
	public void getConsultsForPatient() throws Exception
	{

	}

	@Test
	public void getConsultsForDoctor() throws Exception
	{
		ConsultController controller = new ConsultController();
		List<Consult> dbConsults = controller.getConsultsForDoctor(2);
		assertEquals("dr. test", dbConsults.get(0).getDoctorName());
	}

	@Test
	public void checkIn() throws Exception
	{
		Map<String, String> map = new HashMap<>();
		map.put("doctorID", "2");
		map.put("consultID", "1");
		ConsultController controller = new ConsultController();
		controller.checkIn(map);

		org.hibernate.Session session = SessionDB.getSession();
		DoctorEntity doctor = (DoctorEntity) session.load(DoctorEntity.class, 2);
		assertEquals("busy", doctor.getAvailability());
	}

	@Test
	public void checkOut() throws Exception
	{
		Map<String, String> map = new HashMap<>();
		map.put("doctorID", "2");
		map.put("consultID", "1");
		ConsultController controller = new ConsultController();
		controller.checkOut(map);

		org.hibernate.Session session = SessionDB.getSession();
		DoctorEntity doctor = (DoctorEntity) session.load(DoctorEntity.class, 2);
		assertEquals("free", doctor.getAvailability());
	}
}