package com.clinic.model;

import java.util.Collection;

/**
 * User: Stefa
 * Date: 15-May-16
 * Time: 17:50
 */
public class DoctorEntity
{
    private int doctorId;
    private String doctorName;
    private String availability;
	private UserEntity doctorUser;
    private int userId;

    public int getDoctorId()
    {
        return doctorId;
    }

    public void setDoctorId(int doctorId)
    {
        this.doctorId = doctorId;
    }

    public String getDoctorName()
    {
        return doctorName;
    }

    public void setDoctorName(String doctorName)
    {
        this.doctorName = doctorName;
    }

    public String getAvailability()
    {
        return availability;
    }

    public void setAvailability(String availability)
    {
        this.availability = availability;
    }

	public UserEntity getDoctorUser()
	{
		return doctorUser;
	}

	public void setDoctorUser(UserEntity doctorUser)
	{
		this.doctorUser = doctorUser;
	}

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DoctorEntity that = (DoctorEntity) o;

        if (doctorId != that.doctorId) return false;
        if (doctorName != null ? !doctorName.equals(that.doctorName) : that.doctorName != null) return false;
        if (availability != null ? !availability.equals(that.availability) : that.availability != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = doctorId;
        result = 31 * result + (doctorName != null ? doctorName.hashCode() : 0);
        result = 31 * result + (availability != null ? availability.hashCode() : 0);
        return result;
    }

}
