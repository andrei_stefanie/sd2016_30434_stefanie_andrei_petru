package com.clinic.model;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.sql.Timestamp;

/**
 * User: Stefa
 * Date: 15-May-16
 * Time: 17:50
 */
public class ConsultationEntity
{
    private int consultId;
	@Temporal(TemporalType.TIMESTAMP)
	private Timestamp since;
	@Temporal(TemporalType.TIMESTAMP)
	private Timestamp till;
    private DoctorEntity doctorByDoctorId;
    private PatientEntity patientByPatientId;
	private String description;
	private String status;

    public int getConsultId()
    {
        return consultId;
    }

    public void setConsultId(int consultId)
    {
        this.consultId = consultId;
    }

    public Timestamp getSince()
    {
        return since;
    }

    public void setSince(Timestamp from)
    {
        this.since = from;
    }

    public Timestamp getTill()
    {
        return till;
    }

    public void setTill(Timestamp to)
    {
        this.till = to;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConsultationEntity that = (ConsultationEntity) o;

        if (consultId != that.consultId) return false;
        if (since != null ? !since.equals(that.since) : that.since != null) return false;
        if (till != null ? !till.equals(that.till) : that.till != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = consultId;
        result = 31 * result + (since != null ? since.hashCode() : 0);
        result = 31 * result + (till != null ? till.hashCode() : 0);
        return result;
    }

	@ManyToOne
	@JoinColumn(name = "doctorID")
	public DoctorEntity getDoctorByDoctorId()
    {
        return doctorByDoctorId;
    }

    public void setDoctorByDoctorId(DoctorEntity doctorByDoctorId)
    {
        this.doctorByDoctorId = doctorByDoctorId;
    }

	@ManyToOne
	@JoinColumn(name = "patientID")
	public PatientEntity getPatientByPatientId()
    {
        return patientByPatientId;
    }

    public void setPatientByPatientId(PatientEntity patientByPatientId)
    {
        this.patientByPatientId = patientByPatientId;
    }

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;

	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}
}