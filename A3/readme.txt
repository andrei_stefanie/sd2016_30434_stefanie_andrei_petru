Installation Instructions:

1 Java

1.1 Java JDK and JRE
1) Access the next link:
http://www.oracle.com/technetwork/java/javase/downloads/index.html
2) Click on the icon which is above Java Platform (JDK). You will be redirected to Java
downloads.
3) Click on the link Accept License Agreement.
4) Click on the link which corresponds to your version of the Operating System. In the
example the version which is used corresponds to Windows x64 and the file is named
jdk-8u51-windows-x64.exe.
5) After java-version.exe is pressed, a file with the same name will be downloaded.
6) Click on java-version.exe.
7) You will be asked the next question: Do you want to allow the following program to
make changes to this computer? Click Yes.
8) Click Next.
9) You will be asked where you want to install Java. Use the default location and click Next.
10) After the JDK is installed, you will be asked where you want to install the JRE. Use the
default location and click Next.

1.2 Set JAVA_HOME and JAVA_JRE variables
1) Click Start.
2) Right-Click on Computer.
3) Select Properties.
4) Click on Advanced System Settings.
5) Click on Environment Variables.
6) Under System Variables click New.
7) In the text field associated with the name of the variable insert JAVA_HOME and in the
field associated with the value of the variable insert C:\Program Files\Java\java_version;.
8) Click OK.
9) Under System Variables click New again.
10) In the text field associated with the name of the variable insert JRE_HOME and in the
field associated with the value of the variable insert C:\Program Files\Java\java_version;.
11)  Click OK.


2 WAMP

1) Download WAMP from:
http://www.wampserver.com/en/#download-wrapper  (preferably 64bit version)
2) Next
3) Accept the agreement
4) Select the installation location (can use the default c:\\)
5) Next
6) Next
7) Setup your mail
8) Next (localhost is default)
9) Just press open (it searches for file explorer - "My Computer")
10) Installation is complete, you can start it.
11) It will be used to create a MySQL server and for the front-end part


3 Web Server

3.1 Tomcat
1) Click on the next link: https://tomcat.apache.org/download-70.cgi. (It should work with other versions of tomcat too)
2) Under Binary Distributions look for Core and click on zip.
3) A file called apache-tomcat-version.zip is downloaded.
4) Extract the content of this file on C:\. The file startup.bat should be at the location
C:\apache-tomcat-7.0.24\bin.
3.2 Set the CATALINA_HOME variable
1) Click Start.
2) Right-Click on Computer.
3) Select Properties.
4) Click on Advanced System Settings.
5) Click on Environment Variables.
6) Under System Variables click New.
7) In the text field associated with the name of the variable insert CATALINA_HOME and
in the field associated with the value of the variable insert C:\apache-tomcat-version;.
8) Click OK
9) It will be used to create the server for the back-end part


Setting up the application:
- Start WAMP application (it will appear in the notification area of the taskbar)
- Left click on it
- Go to MySQL and click on MySQL Console (password should be empty)
- Paste the following script in it


4 Create the Bank database - 

MySQL Script:
----------------------------Start of MySQL Scrip------------------------------

CREATE DATABASE IF NOT EXISTS `clinic` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `clinic`;

CREATE TABLE IF NOT EXISTS `consultation` (
  `consultID` int(11) NOT NULL AUTO_INCREMENT,
  `patientID` int(11) NOT NULL,
  `doctorID` int(11) NOT NULL,
  `since` timestamp NULL DEFAULT NULL,
  `till` timestamp NULL DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`consultID`),
  UNIQUE KEY `consultID_UNIQUE` (`consultID`),
  KEY `patient_idx` (`patientID`),
  KEY `doctor_idx` (`doctorID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `doctor` (
  `doctorID` int(11) NOT NULL AUTO_INCREMENT,
  `doctorName` varchar(45) NOT NULL,
  `availability` varchar(10) NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`doctorID`),
  UNIQUE KEY `doctorID_UNIQUE` (`doctorID`),
  KEY `doctorUser_idx` (`userID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `patient` (
  `patientID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `cnp` varchar(13) NOT NULL,
  `birthDate` timestamp NULL DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`patientID`),
  UNIQUE KEY `patientID_UNIQUE` (`patientID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `user` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `type` varchar(10) NOT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `userID_UNIQUE` (`userID`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO `user` (`userID`, `username`, `password`, `type`) VALUES
(4, 'test', 'test', 'admin'),
(11, 'doctorUser', 'test', 'doctor'),
(12, 'testS', 'test', 'secretary'),
(14, 'testD', 'test', 'doctor');


ALTER TABLE `consultation`
  ADD CONSTRAINT `doctorFK` FOREIGN KEY (`doctorID`) REFERENCES `doctor` (`doctorID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `patientFK` FOREIGN KEY (`patientID`) REFERENCES `patient` (`patientID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `doctor`
  ADD CONSTRAINT `doctorUser` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE;

  ---------------------End of MySQL script--------------------------


5 Set up the front-end part
- Open the Front directory from the source files
- Copy the app directory to C:\wamp\www (if wamp was installed in the default location)
- Click on WAMP and Restart All Services


6 Set up the back-end part
- Open the Back directory from the source files
- Copy the Bank directory to C:\apache-tomcat-7.0.65\webapps (if tomcat was installed in the root of C drive)
- Start the Tomcat server by navigating to C:\apache-tomcat-7.0.65\bin and run startup.bat


The application can be used by accessing http://localhost/app/ from a browser
Default manager account:
username: test
password: test

Default secretary account:
username: testS
password: test

Default doctor account:
username: testD
password: test