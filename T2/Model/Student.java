package Lab2.Model;

import java.sql.Date;

/**
 * User: Stefa
 * Date: 14-Mar-16

 * Time: 14:35
 */
public class Student
{
    private int id;
    private String name;
    private Date birthDate;
    private String address;


    public Student(int id, String name, String birthDate, String address)
    {
        this.id = id;
        this.birthDate = Date.valueOf(birthDate);
        this.name = name;
        this.address = address;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Date getBirthDate()
    {
        return birthDate;
    }

    public void setBirthDate(Date birthDate)
    {
        this.birthDate = birthDate;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }
}
