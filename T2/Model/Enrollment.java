package Lab2.Model;

/**
 * User: Stefa
 * Date: 14-Mar-16
 * Time: 14:43
 */
public class Enrollment
{
    private int courseID;
    private int studentID;

    public Enrollment(int courseID, int studentID)
    {
        this.courseID = courseID;
        this.studentID = studentID;
    }

    public int getCourseID()
    {
        return courseID;
    }

    public void setCourseID(int courseID)
    {
        this.courseID = courseID;
    }

    public int getStudentID()
    {
        return studentID;
    }

    public void setStudentID(int studentID)
    {
        this.studentID = studentID;
    }
}
