package Lab2.DAO;

import Lab2.Connect;
import Lab2.Model.Course;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: Stefa
 * Date: 14-Mar-16
 * Time: 14:46
 */
public class CourseDAO
{
    private Connect conn = Connect.getInstance();

    /**
     * Method used to insert a course object into the course table
     * @param course - object defined in the Model layout. It contains all th basic information about a course
     * @return true if the insertion was successful, false otherwise
     */
    public boolean insertCourse(Course course)
    {
        String query = "INSERT INTO course (courseID, name, teacher, study_year) VALUES " +
                "('"+course.getId()+"','"+course.getName()+"','"+course.getTeacher()+"','"+course.getStudyYear()+"')";
        int res = conn.executeUpdate(query);

        return res != 0;
    }

    /**
     * Method used to update one row of the course table
     * @param course - the method will search for the course having the specified ID and will update the rest of the information
     * @return true if success, false otherwise
     */
    public boolean updateCourse(Course course)
    {
        String query = "UPDATE course SET name='"+course.getName()+ "' ,teacher='"+course.getTeacher()+
                "' ,study_year='"+course.getStudyYear()+"' WHERE courseID='"+course.getId()+"'";
        int res = conn.executeUpdate(query);

        return res != 0;
    }

    /**
     * Method used to delete the specified course from the database (course table)
     * @param course - reference to the course entry from the table to be deleted
     * @return true if success, false otherwise
     */
    public boolean deleteCourse(Course course)
    {
        String query = "DELETE FROM course WHERE courseID='"+course.getId()+"'";
        int res = conn.executeUpdate(query);

        return res != 0;
    }

    public Course getCourse(int id)
    {
        String query = "SELECT * FROM course WHERE courseID='" + id + "'";
        ResultSet res = conn.executeQuery(query);

        try
        {
            if(res.next())
            {
                return new Course(Integer.parseInt(res.getString(1)), res.getString(2), res.getString(3), Integer.parseInt(res.getString(4)));
            }
        } catch (SQLException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
