package Lab2.DAO;

import Lab2.Connect;
import Lab2.Model.Student;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: Stefa
 * Date: 14-Mar-16
 * Time: 15:03
 */
public class StudentDAO
{
    private Connect conn = Connect.getInstance();

    /**
     * Method used to insert a student object into the student table
     * @param student - object defined in the Model layout. It contains all th basic information about a Student
     * @return true if the insertion was successful, false otherwise
     */
    public boolean insertStudent(Student student)
    {
        String query = "INSERT INTO Student (name, birthday, address) VALUES " +
                "('"+student.getName()+"','"+student.getBirthDate()+"','"+student.getAddress()+"')";
        int res = conn.executeUpdate(query);

        return res != 0;
    }

    /**
     * Method used to update one row of the student table
     * @param student - the method will search for the Student having the specified ID and will update the rest of the information
     * @return true if success, false otherwise
     */
    public boolean updateStudent(Student student)
    {
        String query = "UPDATE student SET name='"+student.getName()+ "' , birthday='"+student.getBirthDate()+
                "' , address='"+student.getAddress()+"' WHERE studentID='"+student.getId()+"'";
        int res = conn.executeUpdate(query);

        return res != 0;
    }

    /**
     * Method used to delete the specified Student from the database (student table)
     * @param student - reference to the student entry from the table to be deleted
     * @return true if success, false otherwise
     */
    public boolean deleteStudent(Student student)
    {
        String query = "DELETE FROM student WHERE studentID='"+student.getId()+"'";
        int res = conn.executeUpdate(query);

        return res != 0;
    }

    public Student getStudent(int id)
    {
        String query = "SELECT * FROM student WHERE studentID=" + id;
        ResultSet res = conn.executeQuery(query);

        try
        {
            if(res.next())
            {
                return new Student(Integer.parseInt(res.getString(1)), res.getString(2), res.getString(3), res.getString(4));
            }
        } catch (SQLException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
