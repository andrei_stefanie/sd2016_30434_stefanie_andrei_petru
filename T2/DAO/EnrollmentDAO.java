package Lab2.DAO;

import Lab2.Connect;
import Lab2.Model.Course;
import Lab2.Model.Student;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * User: Stefa
 * Date: 14-Mar-16
 * Time: 15:07
 */
public class EnrollmentDAO
{
    private Connect conn = Connect.getInstance();

    /**
     * Method used to insert a new enrollment entry
     * @param course - the course for which the student will be enrolled
     * @param student - the student to be enrolled
     * @return true if success, false otherwise
     */
    public boolean enrollStudent(Student student, Course course)
    {
        String query = "SELECT * FROM enrollment WHERE courseID = " + course.getId() + " AND studentID = " + student.getId();
        try
        {
            if(conn.executeQuery(query).wasNull())
            {
                query = "INSERT INTO enrollment (courseID, studentID) VALUES " +
                        "('" + course.getId() + "','" + student.getId() + "')";
                int res = conn.executeUpdate(query);

                return res != 0;
            }
        } catch (SQLException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Method used to delete one entyr from the enrollment table
     * @param course - the course from which the student will ne unenrolled
     * @param student - the student to unenroll
     * @return true if success, false otherwise
     */
    public boolean unenrollStudent(Student student, Course course)
    {
        String query = "DELETE FROM enrollment WHERE courseID = " + course.getId() + " AND studentID = " + student.getId();
        int res = conn.executeUpdate(query);

        return res != 0;
    }

    /**
     * Method used to obtain a list of all the student that attent a specific course
     * @param course - obtain the students attending this specific course
     * @return array list of students attending that course
     */
    public ArrayList<Student> getStudents(Course course)
    {
        ArrayList<Student> studentList = new ArrayList<>();

        String query = "SELECT s.studentID, name FROM student AS s JOIN enrollment AS e WHERE e.courseID = " + course.getId();
        ResultSet res = conn.executeQuery(query);
        try
        {
            while(res.next())
            {
                Student s = new Student(Integer.parseInt(res.getString(1)), res.getString(2), res.getString(3), res.getString(4));
                studentList.add(s);
            }
        } catch (SQLException e)
        {
            e.printStackTrace();
        }

        return studentList;
    }
}
