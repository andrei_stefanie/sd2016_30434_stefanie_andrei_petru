package Lab2.Test;

import Lab2.DAO.StudentDAO;
import Lab2.Model.Student;
import junit.framework.TestCase;

/**
 * User: Stefa
 * Date: 14-Mar-16
 * Time: 15:58
 */
public class TestUnitStudent extends TestCase
{
    public void testStudent() throws Exception
    {
        StudentDAO studentDAO = new StudentDAO();
        Student student = new Student(7, "junit", "1994-02-11", "whatever street");

        boolean actual = studentDAO.insertStudent(student);
        assertEquals(true, actual);

        student = studentDAO.getStudent(7);

        String actualName = "junit";
        assertEquals(actualName, student.getName());

        String actualAddress = "whatever street";
        assertEquals(actualAddress, student.getAddress());

        student = new Student(7, "junit", "1994-02-11", "new street");

        actual = studentDAO.updateStudent(student);
        assertEquals(true, actual);

        student = studentDAO.getStudent(7);

        Student actualStudent = new Student(7, "junit", "1994-02-11", "new street");
        assertEquals(actualStudent.getAddress(), student.getAddress());

        student = new Student(7, "junit", "1994-02-11", "whatever street");

        actual = studentDAO.deleteStudent(student);
        assertEquals(true, actual);

        student = studentDAO.getStudent(7);

        assertEquals(null, student);
    }
}
