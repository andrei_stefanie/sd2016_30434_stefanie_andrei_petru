package Lab2.Test;

import Lab2.DAO.CourseDAO;
import Lab2.Model.Course;
import junit.framework.TestCase;

/**
 * User: Stefa
 * Date: 14-Mar-16
 * Time: 16:07
 */
public class TestUnitCourse extends TestCase
{
    public void testCourseInsert() throws Exception
    {
        CourseDAO courseDAO = new CourseDAO();
        Course course = new Course(3, "testCourse", "testTeacher", 3);

        boolean actual = courseDAO.insertCourse(course);
        assertEquals(true, actual);

        course = courseDAO.getCourse(3);

        String actualName = "testCourse";
        assertEquals(actualName, course.getName());

        String actualTeacher = "testTeacher";
        assertEquals(actualTeacher, course.getTeacher());

        course = new Course(2, "newTestCourse", "newTestTeacher", 3);

        actual = courseDAO.updateCourse(course);
        assertEquals(true, actual);

        course = courseDAO.getCourse(2);

        actualName = "newTestCourse";
        assertEquals(actualName, course.getName());

        actualTeacher = "newTestTeacher";
        assertEquals(actualTeacher, course.getTeacher());
    }

//
//    public void testCourseDelete() throws Exception
//    {
//        CourseDAO courseDAO = new CourseDAO();
//        Course course = new Course(2, "newTestCourse", "newTestTeacher", 3);
//
//        boolean actual = courseDAO.deleteCourse(course);
//        assertEquals(true, actual);
//    }
//
//    public void testNullCourse() throws Exception
//    {
//        CourseDAO courseDAO = new CourseDAO();
//        Course course = courseDAO.getCourse(2);
//
//        assertEquals(null, course);
//    }
}
