package Lab2;

import java.sql.*;

/**
 * User: Stefa
 * Date: 14-Mar-16
 * Time: 14:11
 */

public class Connect
{
    private static Connect instance = null;
    private Connection conn;

    private Connect()
    {
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "course_management";
        String user = "root";
        String pass = "";
        String driver = "com.mysql.jdbc.Driver";
        try
        {
            Class.forName(driver).newInstance();
            this.conn = DriverManager.getConnection(url + dbName, user, pass);
            System.out.println("New connection created");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public static Connect getInstance()
    {
        if(instance == null)
        {
            instance = new Connect();
        }

        return instance;
    }

    public int executeUpdate(String query)
    {
        int result = 0;

        try
        {
            Statement statement = instance.conn.createStatement();
            result = statement.executeUpdate(query);
            System.out.println("Query executed");
        } catch (SQLException e)
        {
            e.printStackTrace();
        }

        return result;
    }

    public ResultSet executeQuery(String query)
    {
        ResultSet result = null;

        try
        {
            Statement statement = instance.conn.createStatement();
            result = statement.executeQuery(query);
            System.out.println("Query executed");
        } catch (SQLException e)
        {
            e.printStackTrace();
        }

        return result;
    }
}
