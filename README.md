This repository is used to version control assignments for Software Design (TUC-N Y3 S2 2016). All the projects will contain source code, executable code, documentation and readme (with screenshots).

Structure:

A1 - Assignment 1 - Bank Application (MySQL + Hibernate + Spring MVC + AngularJS 1.4.8)
                  - Users: Manager, Front-desk Employee

A2 - Assignment 2 - Book Store Application (XML Data Files + iText + Spring MVC + AngularJS 1.4.8)
                  - Users: Manager, Front-desk Employee

A3 - Assignment 3 - Clinic Application (MySQL + Hibernate + Spring MVC + AngularJS 1.4.8)
                  - Users: Manager, Receptionist, Doctor

P - Semester Project - Online Betting Platform (MySQL + Hibernate + Spring MVC + AngularJS 1.4.8 - will be converted to Angular2)
                     - Users: Manager, Client

T1 - Homework 1 - UML Exercises

T2 - Homework 2 - Testing (Junit) exercise